import React, { useEffect } from "react";
import { useState } from "react";
import { Redirect, Route } from "react-router-dom";

import { IonApp, IonContent, IonPage, IonRouterOutlet } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";

import { AuthState, onAuthUIStateChange } from "@aws-amplify/ui-components";
import Auth from "@aws-amplify/auth";
import { useTranslation } from "react-i18next";
import Header from "core/components/Header";
import Menu from "core/components/Menu";
import Works from "./pages/Works";
import WorkDetail from "./pages/work/WorkDetail";
import Outline from "pages/outline/Outline";
import Notifications from "pages/notifications/Notifications";
import SnackbarMessage from "core/components/SnackbarMessage";
import Mypage from "pages/mypage/Mypage";
import Authentication from "shared/components/Authentication";
import Award from "pages/Award";
import ProtectedRoute from "core/components/ProtectedRoute";
import Apply from "pages/apply/Apply";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import { ThemeProvider } from "@mui/styles";
import { createTheme, StyledEngineProvider, Theme } from "@mui/material";
import Spacer from "shared/components/Spacer";

declare module "@mui/styles/defaultTheme" {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

const App: React.FC = () => {
  // menu
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const toggleMenu = () => {
    menuIsOpen ? setMenuIsOpen(false) : setMenuIsOpen(true);
  };
  const [workTitle, setWorkTitle] = useState<string>("");

  // authentication
  const [authState, setAuthState] = useState<AuthState>(AuthState.Loading);
  const [user, setUser] = React.useState<any>();
  useEffect(() => {
    const getAuthState = async () => {
      // get auth state
      const userInfo = await Auth.currentUserPoolUser({ bypassCache: true });
      // ログインしていればAuth infoをセット
      if (userInfo) {
        setUser(userInfo);
        setAuthState(AuthState.SignedIn);
      } else {
        setAuthState(AuthState.SignedOut);
      }
    };
    getAuthState();
    // watch auth state changes
    return onAuthUIStateChange((nextAuthState, authData) => {
      setAuthState(nextAuthState);
      setUser(authData);
    });
  }, []);

  // snackbar
  const [isSnackbarOpen, setIsSnackbarOpen] = React.useState<boolean>(false);
  const [snackbarMessage, setSnackbarMessage] = React.useState<null | string>(
    null
  );
  const openSnackbar = (message: string) => {
    setSnackbarMessage(message);
    setIsSnackbarOpen(true);
  };
  const closeSnackbar = () => {
    setIsSnackbarOpen(false);
  };

  const theme = createTheme();

  // language
  const { t, i18n } = useTranslation();
  const [lang, setLang] = useState("ja");
  useEffect(() => {
    i18n.changeLanguage(lang);
  }, [lang, i18n]);

  const [nfCount, setNfCount] = useState<number>(0);
  useEffect(() => {
    if (user && user.attributes["custom:notificationsAmount"]) {
      setNfCount(Number(user.attributes["custom:notificationsAmount"]));
    }
  }, [, user]);

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <IonApp>
          <IonReactRouter>
            <Header
              nfCount={nfCount}
              toggleMenu={toggleMenu}
              workTitle={workTitle}
              t={t}
            />
            <Menu
              nfCount={nfCount}
              authState={authState}
              user={user}
              menuIsOpen={menuIsOpen}
              toggleMenu={toggleMenu}
              t={t}
              lang={lang}
              setLang={setLang}
            />
            <IonRouterOutlet>
              <Route exact path="/auth">
                <Authentication />
              </Route>
              <Route exact path="/award">
                <IonPage>
                  <Spacer />
                  <IonContent fullscreen>
                    <Award t={t} />
                  </IonContent>
                </IonPage>
              </Route>
              <Route exact path="/outline">
                <Outline t={t} lang={lang} />
              </Route>
              <Route exact path="/works">
                <Works t={t} />
              </Route>
              <Route path="/work/:workId" exact>
                <WorkDetail
                  setWorkTitle={setWorkTitle}
                  authState={authState}
                  user={user}
                  openSnackbar={openSnackbar}
                />
              </Route>
              <ProtectedRoute authState={authState} path="/mypage" exact>
                <Mypage user={user} t={t} />
              </ProtectedRoute>
              <ProtectedRoute authState={authState} path="/apply" exact>
                <Apply user={user} openSnackbar={openSnackbar} t={t} />
              </ProtectedRoute>
              <ProtectedRoute authState={authState} path="/notifications" exact>
                <Notifications setNfCount={setNfCount} user={user} />
              </ProtectedRoute>
              {/* 参加URLとして/entranceを宣伝しているため */}
              <Route exact path="/entrance">
                <Redirect to="/outline" />
              </Route>
              <Route exact path="/">
                <Redirect to="/works" />
              </Route>
            </IonRouterOutlet>
          </IonReactRouter>
          <SnackbarMessage
            message={snackbarMessage}
            isOpen={isSnackbarOpen}
            close={closeSnackbar}
          />
        </IonApp>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export default App;
