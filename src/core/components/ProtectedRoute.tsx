import { Redirect, Route, RouteProps } from "react-router";

import { AuthState } from "@aws-amplify/ui-components";

interface props extends RouteProps {
  authState: AuthState;
  path: string;
}

// ログイン済みなら子コンポーネントにアクセス
// 未ログインならログイン画面にリダイレクト
const ProtectedRoute = ({ authState, path, ...rest }: props) => {
  if (authState === AuthState.SignedIn) {
    return <Route path={path} {...rest} />;
  } else {
    return (
      <Route>
        {/* 元のページのパスを渡し、認証後に元のページに戻るように */}
        <Redirect
          to={{
            pathname: "/auth",
            state: {
              goBack: true,
              previousUrl: path,
            },
          }}
        />
      </Route>
    );
  }
};
export default ProtectedRoute;
