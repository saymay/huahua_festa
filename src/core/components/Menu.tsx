import {
  Badge,
  Box,
  Divider,
  Drawer,
  FormControl,
  InputLabel,
  List,
  ListItem,
  ListItemText,
  MenuItem,
  Paper,
  Select,
} from "@mui/material";
import { AuthState } from "@aws-amplify/ui-components";
import { Auth } from "aws-amplify";
import useRouteTo from "shared/functions/useRouteTo";

interface menuProps {
  nfCount: number;
  authState: AuthState;
  user: any;
  menuIsOpen: boolean;
  toggleMenu: (event: any) => void;
  t: (text: string) => string;
  lang: string;
  setLang: (value: string) => void;
}

interface Route {
  path: string;
  name: string;
}

// TODO: 結果発表時にawardへのリンクを有効化
const routes: Route[] = [
  {
    path: "/award",
    name: "結果発表",
  },
  {
    path: "/outline",
    name: "フェスタ概要",
  },
  {
    path: "/apply",
    name: "作品応募",
  },
  {
    path: "/works",
    name: "作品一覧",
  },
  {
    path: "/mypage",
    name: "マイページ",
  },
];

const Menu = ({
  nfCount,
  menuIsOpen,
  toggleMenu,
  t,
  lang,
  setLang,
}: menuProps) => {
  // routing
  const routeTo = useRouteTo();

  // sign out
  const signOut = () => {
    Auth.signOut();
  };

  // language
  const handleChange = (event: any) => {
    setLang(event.target.value as string);
  };

  return (
    <Drawer anchor="right" open={menuIsOpen} onClick={toggleMenu}>
      <List>
        {routes.map((route) => (
          <Paper elevation={0} key={route.path}>
            <ListItem
              key={route.path}
              button
              onClick={() => {
                routeTo(route.path);
              }}
            >
              <ListItemText primary={t(route.name)} />
            </ListItem>
            <Divider />
          </Paper>
        ))}
        {/* 通知一覧 */}
        <ListItem
          button
          onClick={() => {
            routeTo("/notifications");
          }}
        >
          <ListItemText>
            <Badge badgeContent={nfCount} color="primary">
              {t("通知一覧")}
            </Badge>
          </ListItemText>
        </ListItem>
        <Divider />
        {/* 言語切り替え */}
        <ListItem>
          <FormControl>
            <InputLabel id="language">{t("言語")}</InputLabel>
            <Select labelId="product" value={lang} onChange={handleChange}>
              <MenuItem value={"ja"}>日本語</MenuItem>
              <MenuItem value={"en"}>English</MenuItem>
              <MenuItem value={"zh"}>中文</MenuItem>
            </Select>
          </FormControl>
        </ListItem>
        <Divider />
        {/* ログアウト */}
        <ListItem button onClick={signOut}>
          <ListItemText>{t("ログアウト")}</ListItemText>
        </ListItem>
        <Divider />
      </List>
    </Drawer>
  );
};

export default Menu;
