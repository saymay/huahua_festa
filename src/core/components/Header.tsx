import { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router";
import { AppBar, Badge, Button, Toolbar, Typography } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

type headerProps = {
  nfCount: number;
  toggleMenu: (event: any) => void;
  workTitle: string;
  t: (text: string) => string;
};

const Header = ({ nfCount, toggleMenu, workTitle, t }: headerProps) => {
  const pathTitleMap: string[][] = [
    ["/award", "結果発表"],
    ["/mypage", "マイページ"],
    ["/outline", "フェスタ概要"],
    ["/works", "作品一覧"],
    ["/home", "作品一覧"],
    ["/apply", "作品応募"],
    ["/notifications", "通知一覧"],
    ["/work/", ""],
  ];

  /**
   * @description 現在のパスからメニューのタイトルを求める
   *
   * @param {string} pathname
   * @return {*}  {string}
   */
  const getTitle = (pathname: string): string => {
    const pathTitle = pathTitleMap.find(([path, _]) =>
      pathname.startsWith(path)
    );
    if (pathTitle) {
      if (pathTitle[0] === "/work/") {
        return workTitle;
      } else {
        return pathTitle[1];
      }
    } else return "";
  };

  // go back to previous page
  const history = useHistory();
  const goBack = () => history.goBack();

  // パスの変化とworkTitleの変更を検知し、メニューのタイトルを更新
  const [title, setTitle] = useState(getTitle(useLocation().pathname));
  history.listen((location, action) => {
    setTitle(getTitle(location.pathname));
  });
  const location = useLocation();
  useEffect(() => {
    setTitle(getTitle(location.pathname));
  }, [workTitle]);

  return (
    <AppBar
      position="static"
      color="default"
      sx={{ backgroundColor: "transparent", boxShadow: "none", zIndex: 1 }}
    >
      <Toolbar>
        <Button
          variant="contained"
          onClick={goBack}
          sx={{ backgroundColor: "white", borderRadius: 30 }}
        >
          <ArrowBackIcon sx={{ color: "black" }} />
        </Button>
        <Typography
          variant="h6"
          sx={{
            flexGrow: 1,
            textAlign: "center",
            mr: 2.5,
            ml: 2.5,
            borderRadius: 30,
            backgroundColor: "white",
            boxShadow:
              "0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)",
            lineHeight: 2.6,
            fontSize: 14,
          }}
        >
          {t(title) ? t(title) : title}
        </Typography>
        <Button
          variant="contained"
          onClick={toggleMenu}
          sx={{ backgroundColor: "white", borderRadius: 30 }}
        >
          <Badge badgeContent={nfCount} color="primary">
            <MenuIcon sx={{ color: "black" }} />
          </Badge>
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
