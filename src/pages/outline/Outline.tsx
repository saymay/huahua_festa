import { Link } from "react-router-dom";
import { IonContent, IonPage } from "@ionic/react";
import { Button, Container, Grid } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import useRouteTo from "shared/functions/useRouteTo";
import Spacer from "shared/components/Spacer";
import banner from "shared/assets/images/banner.jpg";
import AwardMessage from "shared/components/AwardMessage";

// styles
const useStyles = makeStyles(() =>
  createStyles({
    banner: {
      display: "flex",
      justifyContent: "center",
    },
    button: {
      marginBottom: 20,
      marginRight: 5,
      marginLeft: 5,
      backgroundColor: "white",
      color: "black",
      border: "solid 1px #e0e0e0",
    },
  })
);

interface props {
  t: (text: string) => string;
  lang: string;
}

const Outline = ({ t, lang }: props) => {
  const classes = useStyles();
  const routeTo = useRouteTo();
  const apply = () => {
    routeTo("/apply");
  };

  return (
    <IonPage>
      <IonContent fullscreen>
        <Spacer />
        <Container>
          <section className={classes.banner}>
            <img src={banner} alt="HUAHUA Art Festival" />
          </section>

          <AwardMessage />
          {lang === "ja" ? (
            <div>
              <section>
                <p>
                  本企画は画家の方々の活動を応援するもので、アートを楽しむ機会をつくるためアート作品を募集します。
                  ご自身のアート作品を投稿してください。
                </p>
                <p>
                  応募作品の中から、当フェスタをご覧いただく一般のユーザの方による投票をおこないます。
                  応募作品の「いいね」の得票数により、各賞と賞品の受賞者を決定します。受賞者には
                  eメールタイプのAmazonギフト券 をプレゼントさせていただきます。
                  作品へのコメントの賞も用意しているので、ぜひコメントも投稿してみてください。
                </p>
              </section>
              <section>
                <ul>
                  <li>
                    テーマ：　テーマ指定はありません。皆様の個性が光るテーマで投稿してください。
                  </li>
                  <li>応募期間：10月2日（土）〜</li>
                  <li>フェスタ期間：10月16日（土）〜 11月13日（土）</li>
                  <li>結果発表：11月14日（日）</li>
                </ul>
              </section>
              <section>
                <h2>参加方法</h2>
                <ol>
                  <li>
                    <Link to="/apply">こちら</Link>
                    のリンクからフェスタに参加登録をおこなう
                  </li>
                  <li>投稿する作品の画像ファイルを選択する</li>
                  <li>写真をアップロードし、投稿内容を確認する</li>
                  <li>
                    作品の投稿完了です。フェスタの開始日までしばらくお待ちください
                  </li>
                </ol>
              </section>
              <section>
                <h2>賞品</h2>
                <ul>
                  <li>
                    最優秀賞 :
                    フェスタ期間中の「いいね」が１番多かったアカウント
                    <ul>
                      <li>HUAHUA常設展での個展1年間の無料チケット</li>
                      <li>HUAHUAメンバーとインスタライブ</li>
                      <li>Amazonギフト券10,000円分</li>
                    </ul>
                  </li>
                  <li>優秀賞</li>
                  <ul>
                    <li>HUAHUA常設展での個展6か月の無料チケット</li>
                    <li>HUAHUAメンバーとインスタライブ</li>
                    <li>Amazonギフト券5,000円分</li>
                  </ul>
                  <li>
                    HUAHUA賞
                    <ul>
                      <li>HUAHUA常設展での個展6か月の無料チケット</li>
                      <li>HUAHUAメンバーとインスタライブ</li>
                      li Amazonギフト券5,000円分
                    </ul>
                  </li>
                  <li>
                    グッドコメント賞
                    <ul>
                      <li>Amazonギフト券5,000円分 </li>
                      <li>
                        HUAHUA運営による合議によって、興味深いコメントが1件選出されます。
                      </li>
                    </ul>
                  </li>
                </ul>
              </section>

              <section>
                <h2>注意事項</h2>
                以下の項目をすべてお読みの上、あらかじめご了承いただいてから投稿頂きますようお願いいたします。
                <ul>
                  <li>
                    ※当社、合同会社HUAHUAが運営するHUAHUA公式Twitterなど他のSNSや媒体に、参加作品やユーザー名を掲載させていただく場合があります。
                  </li>
                  <li>※おひとり(1アカウント)につき１作品だけ応募可能です。</li>
                  <li>※日本国外からの投稿も可能です。</li>
                  <li>
                    ※アイディアやデザイン等を含め、すべてを自身で創作したオリジナル作品を投稿してください。
                  </li>
                  <li>
                    ※受賞者の方へはメールにてeメールタイプのAmazonギフト券をお送りさせて頂きます。
                  </li>
                  <li>
                    ※フェスタへの応募者ならびにコメント投稿者はHUAHUA利用規約およびHUAHUAプライバシー・ポリシーに同意したものとみなされます。公序良俗に反する作品や犯罪を助長するような作品(や投稿)をはじめ、HUAHUA利用規約において投稿禁止とさせていただいている作品(や投稿)に関しては、削除させていただく場合があります。
                  </li>
                </ul>
              </section>

              <section>
                <h2>FAQ</h2>
                <ul>
                  <li>
                    Q. 平面作品のみですか。A.
                    平面・立体はといません。ただし掲載は画像ファイルによるものになります。
                  </li>
                  <li>
                    Q. 写真作品は投稿できますか。　A.絵画のみならず、
                    写真作品も投稿することができます。
                  </li>
                </ul>
              </section>
            </div>
          ) : null}

          {lang === "en" ? (
            <div>
              <section>
                <p>
                  This project supports the work of painters, and invites you to
                  submit your work of art to create opportunities to enjoy
                  art.We invite you to submit your own artworks.
                </p>
                <p>
                  All visitors who visit Festa will vote by “likes” among all
                  the submissions. The winners will be chosen based on the
                  number of “likes” received for their entries, and will receive
                  an Amazon gift card by e-mail. There will also be a prize for
                  the best comment. Make sure you post your comments on the
                  event platform!
                </p>
              </section>
              <section>
                <ul>
                  <li>
                    Themes: There are no specific themes. Please submit a theme
                    that reflects your personality. Photos will be used for
                    publication.
                  </li>
                  <li>
                    Entry period : Saturday 2nd October ~ Friday 15th October
                  </li>
                  <li>
                    Festa period : Saturday 16th October ~ Saturday 13th
                    November
                  </li>
                  <li>Announcement of results : Sunday 14th November</li>
                </ul>
              </section>
              <section>
                <h2>How to participate</h2>
                <ol>
                  <li>
                    Register your entry at this
                    <Link to="/apply"> Link</Link>. Upload a photo of your work
                  </li>
                  <li>Select the image file of the work to be posted</li>
                  <li>Upload a photo and check the posted content</li>
                  <li>
                    The submission of the work is complete. Please wait for a
                    while until the start date of the festival
                  </li>
                </ol>
              </section>
              <section>
                <h2>Prizes</h2>
                <ul>
                  <li>
                    Grand Prize : The account with the most “likes” during the
                    period
                    <ul>
                      <li>
                        Free ticket for one year of solo exhibition at HUAHUA
                      </li>
                      <li>InstaLive with HUAHUA members</li>
                      <li>Amazon gift certificate worth 10,000 yen</li>
                    </ul>
                  </li>
                  <li>
                    Excellence Prize : The account with the second most “likes”
                    during the period
                  </li>
                  <ul>
                    <li>
                      Free ticket for half year of solo exhibition at HUAHUA
                    </li>
                    <li>InstaLive with HUAHUA members</li>
                    <li>Amazon gift certificate worth 5,000 yen</li>
                  </ul>
                  <li>
                    HUAHUA Prize : Voting by all members of HUAHUA
                    <ul>
                      <li>
                        Free ticket for half year of solo exhibition at HUAHUA
                      </li>
                      <li>InstaLive with HUAHUA members</li>
                      <li>Amazon gift certificate worth 5,000 yen</li>
                    </ul>
                  </li>
                  <li>
                    Good Comment Prize : Discussion by all members of HUAHUA
                    <ul>
                      <li>Amazon gift certificate worth 1,000 yen</li>
                      <li>
                        One interesting comment will be selected by a consensus
                        run by HUAHUA.
                      </li>
                    </ul>
                  </li>
                </ul>
              </section>

              <section>
                <h2>Precautions</h2>I have read and accepted the terms as below
                before my submission.
                <ul>
                  <li>
                    We may publish your entry and user name on other SNS and
                    media such as HUAHUA official Twitter operated by us and
                    HUAHUA LLC.
                  </li>
                  <li>You can only submit one entry per account.</li>
                  <li>Submissions from outside Japan are also accepted.</li>
                  <li>
                    Please submit your original work, including ideas and
                    designs, all of which you have created yourself.
                  </li>
                  <li>
                    The winner will receive an email with an Amazon gift
                    certificate.
                  </li>
                  <li>
                    By entering the festival and submitting comments, you agree
                    to the HUAHUA Terms of Use and the HUAHUA Privacy Policy. We
                    reserve the right to remove any submissions that are
                    offensive, incriminating, or otherwise prohibited by the
                    HUAHUA Terms of Use.
                  </li>
                </ul>
              </section>

              <section>
                <h2>FAQ</h2>
                <ul>
                  <li>
                    Q. Can I submit three-dimensional works? A. Yes, but only
                    photos will be used for publication.
                  </li>
                  <li>
                    Q. Can I submit a photograph? 　A. Yes, you can submit a
                    photograph as well as a painting.
                  </li>
                </ul>
              </section>
            </div>
          ) : null}

          {lang === "zh" ? (
            <div>
              <section>
                <p>
                  这个项目是为了支持画家的活动，创造一个享受艺术的机会。
                  请提交你自己的艺术作品。
                </p>
                <p>
                  你可以提交自己的艺术作品，这些作品将由参观艺术节的公众投票决定。
                  奖金和奖品的获得者将根据其作品收到的 "喜欢 "数量来选择。
                  获奖者将以电子邮件的形式收到一张亚马逊礼券。
                  对参赛作品的评论也会有奖励，所以也请大家踊跃提交你的评论。
                </p>
              </section>
              <section>
                <ul>
                  <li>
                    主题：没有具体的主题。 请提交一个反映你个性的主题。
                    图片将被用于出版。
                  </li>
                  <li>
                    参赛时间：10月2日（星期六）至10月15日（星期五）（所有参赛作品将一次性公布）。
                  </li>
                  <li>节日期间：10月16日（星期六）-11月13日（星期六）。</li>
                  <li>公布结果：11月14日（星期日）</li>
                </ul>
              </section>
              <section>
                <h2>如何参与</h2>
                <ol>
                  <li>
                    通过此链接注册参加 Festa
                    <Link to="/apply">Link</Link>
                  </li>
                  <li>选择要发布的作品的图片文件</li>
                  <li>上传照片并检查发布的内容</li>
                  <li>作品提交完成。 请稍等片刻，直到节日的开始日期</li>
                </ol>
              </section>
              <section>
                <h2>奖品</h2>
                <ul>
                  <li>
                    大奖：节日期间拥有最多 "赞 "的账户
                    <ul>
                      <li>赠送HUAHUA常年展览一年的个展门票</li>
                      <li>与HUAHUA成员进行Insta-live</li>
                      <li>价值10,000日元的亚马逊礼券</li>
                    </ul>
                  </li>
                  <li>优秀奖：节日期间获得第二多 "赞 "的账户</li>
                  <ul>
                    <li>赠送HUAHUA常年展览6个月的个展门票</li>
                    <li>与HUAHUA成员进行Insta-live</li>
                    <li>价值5,000日元的亚马逊礼券</li>
                  </ul>
                  <li>
                    HUAHUA奖：由5位HUAHUA管理层成员投票选出。
                    <ul>
                      <li>赠送HUAHUA永久展览的6个月的展览门票</li>
                      <li>H与HUAHUA成员进行Insta-live</li>
                      <li>价值5,000日元的亚马逊礼券</li>
                    </ul>
                  </li>
                  <li>
                    好评奖：由HUAHUA全体员工组成的理事会
                    <ul>
                      <li>价值1,000日元的亚马逊礼券</li>
                      <li>一个有趣的评论将由花花运行的共识选出。</li>
                    </ul>
                  </li>
                </ul>
              </section>

              <section>
                <h2>笔记</h2>
                在提交作品之前，请阅读并同意以下内容。
                <ul>
                  <li>
                    我们可能会在其他SNS和媒体上公布您的参赛作品和用户名，例如由我们和HUAHUA
                    LLC运营的HUAHUA官方Twitter。
                  </li>
                  <li>每个账户只能提交一个作品。</li>
                  <li>也接受来自日本以外的投稿。</li>
                  <li>
                    请提交你的原创作品，包括创意和设计，都是你自己创造的。
                  </li>
                  <li>获奖者将收到一封电子邮件，内含一张亚马逊礼券。</li>
                  <li>
                    进入艺术节并提交评论，即表示您同意HUAHUA的使用条款和HUAHUA的隐私政策。
                    我们保留删除任何具有攻击性、犯罪性或其他被HUAHUA使用条款所禁止的提交内容的权利。
                  </li>
                </ul>
              </section>

              <section>
                <h2>常见问题</h2>
                <ul>
                  <li>
                    问：你们只接受二维作品吗？
                    　　　答：不，我们同时接受二维和三维作品，但只有照片会被发表。
                  </li>
                  <li>
                    问：我可以提交照片吗？
                    答：是的，你可以提交照片，也可以提交画作。
                  </li>
                </ul>
              </section>
            </div>
          ) : null}

          <Grid container justifyContent="center">
            <Button
              variant="contained"
              onClick={() => {
                routeTo("/works");
              }}
              className={classes.button}
            >
              {t("応募作品を見る")}
            </Button>
            <Button
              variant="contained"
              onClick={() => {
                apply();
              }}
              className={classes.button}
            >
              {t("フェスタに参加登録する")}
            </Button>
          </Grid>
        </Container>
      </IonContent>
    </IonPage>
  );
};

export default Outline;
