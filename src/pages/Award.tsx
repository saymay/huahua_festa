import { IonContent, IonPage } from "@ionic/react";
import { Container, Paper } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import { blue } from "@mui/material/colors";
import Spacer from "shared/components/Spacer";
import useRouteTo from "shared/functions/useRouteTo";
import { Theme } from "@mui/system";

interface props {
  t: (text: string) => string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    link: {
      cursor: "pointer",
      color: blue[700],
      textDecoration: "underline",
      display: "inline-block",
    },
    text: {
      fontFamily: "serif",
    },
    message: {
      [theme.breakpoints.down("sm")]: {
        fontSize: "14px",
      },
    },
  })
);

const Award = ({ t }: props) => {
  const classes = useStyles();
  const routeTo = useRouteTo();

  return (
    <div>
      <Container sx={{ height: "100%", textAlign: "center" }}>
        <p className={classes.message}>
          HUAHUA Art Festival
          投票期間は終了いたしました。たくさんの応募をいただきありがとうございました。
          <a
            className={classes.link}
            onClick={() => {
              routeTo("/outline");
            }}
          >
            賞,フェスタ概要についてはこちら
          </a>
        </p>

        <h2 className={classes.text}>{t("最優秀賞")}</h2>
        <Paper
          elevation={0}
          onClick={() => {
            routeTo("/work/2f624c86-c600-4480-9ae5-053033404847");
          }}
          className={classes.link}
        >
          <h3>Dearest</h3>
          <h5>ryuichiさん</h5>
        </Paper>
        <h2 className={classes.text}>{t("優秀賞")}</h2>
        <Paper
          elevation={0}
          onClick={() => {
            routeTo("/work/9a6e379a-75e1-4db5-9ca3-de14377078c4");
          }}
          className={classes.link}
        >
          <h3>光浴</h3>
          <h5>常田メロンさん</h5>
        </Paper>
        <h2 className={classes.text}>{t("HUAHUA賞")}</h2>
        <Paper
          elevation={0}
          onClick={() => {
            routeTo("/work/923cedb4-1510-40ff-893b-0c85d6a9aecf");
          }}
          className={classes.link}
        >
          <h3>生まれる</h3>
          <h5>書の香さん</h5>
        </Paper>
        <h2 className={classes.text}>{t("グッドコメント賞")}</h2>
        <Paper
          elevation={0}
          onClick={() => {
            routeTo("/work/d98eb118-265c-4b3a-b077-f1401fcf8c92"); // コメントした作品へのリンクを設定
          }}
          className={classes.link}
        >
          <h5>
            「顔がとてもやさしい狐ですね。思わずほおずりしたくなります！」遊狐〜yuko〜
          </h5>
          <h5>hirayam.mkさん</h5>
        </Paper>
      </Container>
    </div>
  );
};

export default Award;
