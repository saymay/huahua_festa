import { useEffect, useState } from "react";
import { useParams } from "react-router";

import { IonContent, IonPage } from "@ionic/react";

import { AuthState } from "@aws-amplify/ui-components";
import { Backdrop, CircularProgress, Drawer } from "@mui/material";

import API from "@aws-amplify/api";
import { graphqlOperation } from "aws-amplify";
import { getLikesByWorkId, getWork } from "graphql/queries";
import { createLikeAndNotification, deleteLike } from "graphql/mutations";
import { Work, Like } from "API";

import WorkGraphic from "../../shared/components/work-graphic/WorkGraphic";
import ActionPanel from "./ActionPanel";
import CommentPanel from "./comment/CommentPanel";
import ShareWork from "./share-work/ShareWork";
import { Anchor } from "./Models";
import useGoAuth from "shared/functions/useGoAuth";

interface props {
  authState: AuthState;
  user: any;
  setWorkTitle: (title: string) => void;
  openSnackbar: (message: string) => void;
}

const WorkDetail = ({ authState, user, setWorkTitle, openSnackbar }: props) => {
  const { workId } = useParams<{ workId: string }>(); // URLパラメータからworkIdを取得
  const [work, setWork] = useState<Work>();
  const [liked, setLiked] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const goAuth = useGoAuth();

  useEffect(() => {
    getWorkData();
    getLikeData();
  }, []);

  // get work data
  const getWorkData = async () => {
    const res = (await API.graphql(
      graphqlOperation(getWork, {
        id: workId,
      })
    )) as any;
    setWork(res.data.getWork);
    setWorkTitle(res.data.getWork.title);
  };

  // get like when authState changes
  // NOTE: ページ読み込み時点ではauthStateがloadingの場合があるため、authStateの変更も検知してトリガーする必要がある
  useEffect(() => {
    getLikeData();
  }, [authState]);

  const getLikeData = async () => {
    if (authState === AuthState.SignedIn) {
      const res = (await API.graphql(
        graphqlOperation(getLikesByWorkId, {
          workId: workId,
        })
      )) as any;
      const likes = res.data.getLikesByWorkId.items;
      const myLike = likes.filter(
        (like: Like) => like.username === user.username
      );
      if (myLike.length > 0) {
        setLiked(true);
      }
    }
  };

  const toggleLike = () => {
    if (authState === AuthState.SignedIn) {
      if (liked) {
        onDeleteLike();
      } else {
        onCreateLike();
      }
      setLiked(!liked);
    } else {
      goAuth();
    }
  };

  const onCreateLike = async () => {
    if (authState === AuthState.SignedIn) {
      const res = await API.graphql(
        graphqlOperation(createLikeAndNotification, {
          workId: workId,
          username: user.username,
        })
      );
    } else {
      goAuth();
    }
  };

  const onDeleteLike = async () => {
    const res = await API.graphql(
      graphqlOperation(deleteLike, {
        input: {
          workId: workId,
          username: user.username,
        },
      })
    );
  };

  // handle drawers
  const [drawersState, setDrawersState] = useState({
    comment: false,
    share: false,
  });
  const toggleDrawer = (anchor: Anchor, open: boolean) => {
    setDrawersState({ ...drawersState, [anchor]: open });
  };

  return (
    <IonPage>
      <IonContent fullscreen>
        {/* ローディング */}
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={isLoading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        {/* 作品グラフィック */}
        {work && (
          <WorkGraphic
            imagePath={work.imagePath}
            aspectRatio={work.aspectRatio}
            frame={work.frame}
            background={work.background}
            lightPosition={work.lightPosition}
            lightIntensity={1}
            setIsLoading={setIsLoading}
          />
        )}
        {/* 操作パネル */}
        <ActionPanel
          liked={liked}
          toggleLike={toggleLike}
          toggleDrawer={toggleDrawer}
        />
        {/* コメントパネル */}
        <Drawer
          anchor="bottom"
          open={drawersState["comment"]}
          onClose={() => {
            toggleDrawer("comment", false);
          }}
        >
          <CommentPanel
            workId={workId}
            toggleDrawer={toggleDrawer}
            authState={authState}
            user={user}
          />
        </Drawer>
        {/* 作品シェア */}
        <Drawer
          anchor="bottom"
          open={drawersState["share"]}
          onClose={() => {
            toggleDrawer("share", false);
          }}
        >
          {work && (
            <ShareWork
              work={work}
              toggleDrawer={toggleDrawer}
              openSnackbar={openSnackbar}
            />
          )}
        </Drawer>
      </IonContent>
    </IonPage>
  );
};

export default WorkDetail;
