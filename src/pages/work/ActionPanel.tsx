import { IconButton } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import AddCommentIcon from "@mui/icons-material/AddComment";
import ShareIcon from "@mui/icons-material/Share";

import { Anchor } from "./Models";

interface props {
  liked: boolean;
  toggleLike: () => void;
  toggleDrawer: (anchor: Anchor, open: boolean) => void;
}

const ActionPanel = ({ liked, toggleLike, toggleDrawer }: props) => {
  // styles
  const useStyles = makeStyles(() =>
    createStyles({
      actionPanel: {
        position: "fixed",
        bottom: 30,
        right: 30,
        zIndex: 5,
      },
    })
  );
  const classes = useStyles();

  return (
    <div className={classes.actionPanel}>
      <div>
        <IconButton onClick={toggleLike} size="large">
          {liked ? (
            <FavoriteIcon sx={{ color: "red" }} />
          ) : (
            <FavoriteBorderIcon sx={{ color: "red" }} />
          )}
        </IconButton>
      </div>
      <div>
        <IconButton
          onClick={() => {
            toggleDrawer("comment", true);
          }}
          size="large"
        >
          <AddCommentIcon />
        </IconButton>
      </div>
      <div>
        <IconButton
          onClick={() => {
            toggleDrawer("share", true);
          }}
          size="large"
        >
          <ShareIcon />
        </IconButton>
      </div>
    </div>
  );
};

export default ActionPanel;
