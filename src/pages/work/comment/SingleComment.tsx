import { Card, CardContent, Theme } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import timestampToDisplayTime from "shared/functions/timestampToDisplayTime";

interface props {
  name: string;
  content: string;
  timestamp: number;
}

const SingleComment = ({ name, content, timestamp }: props) => {
  // styles
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      name: {
        fontSize: 14,
      },
      time: {
        fontSize: 12,
      },
    })
  );
  const classes = useStyles();

  return (
    <Card sx={{ mt: 1 }}>
      <CardContent>
        <div className={classes.name}>{name}</div>
        <div className={classes.time}>{timestampToDisplayTime(timestamp)}</div>
        <div>{content}</div>
      </CardContent>
    </Card>
  );
};

export default SingleComment;
