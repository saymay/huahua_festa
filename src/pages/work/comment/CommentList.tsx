import { useEffect, useState } from "react";

import { graphqlOperation } from "aws-amplify";
import API from "@aws-amplify/api";
import { Comment, ModelSortDirection } from "API";
import { listCommentsByWorkId } from "graphql/queries";
import { onCreateComment } from "graphql/subscriptions";

import SingleComment from "./SingleComment";

interface props {
  workId: string;
}

const CommentList = ({ workId }: props) => {
  const [comments, setComments] = useState<Comment[]>([]);

  useEffect(() => {
    getComments();
    return () => commentsSubscription.unsubscribe();
  }, []);

  // コメントデータの取得
  const getComments = async () => {
    const res = (await API.graphql(
      graphqlOperation(listCommentsByWorkId, {
        workId: workId,
        sortDirection: ModelSortDirection.DESC,
      })
    )) as any;
    const comments = res.data.listCommentsByWorkId.items;
    setComments(comments);
  };

  // commentsの追加を監視
  const commentsSubscription = (
    API.graphql(graphqlOperation(onCreateComment)) as any
  ).subscribe({
    next: (res: any) => {
      const comment = res.value.data.onCreateComment;
      setComments([comment, ...comments]);
    },
  });

  return (
    <div>
      {comments &&
        comments.map((comment) => (
          <SingleComment
            key={comment.id}
            name={comment.username}
            content={comment.content}
            timestamp={comment.createdAt}
          />
        ))}
    </div>
  );
};

export default CommentList;
