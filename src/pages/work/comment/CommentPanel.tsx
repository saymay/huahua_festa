import { Theme } from "@mui/material";

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

import { AuthState } from "@aws-amplify/ui-components";

import { Anchor } from "../Models";
import CommentList from "./CommentList";
import CreateComment from "./CreateComment";

interface props {
  workId: string;
  toggleDrawer: (anchor: Anchor, open: boolean) => void;
  authState: AuthState;
  user: any;
}

const CommentPanel = ({ workId, toggleDrawer, authState, user }: props) => {
  // styles
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      commentPanel: {
        padding: theme.spacing(1),
      },
    })
  );
  const classes = useStyles();

  return (
    <div className={classes.commentPanel}>
      <CreateComment
        workId={workId}
        toggleDrawer={toggleDrawer}
        authState={authState}
        user={user}
      />
      <CommentList workId={workId} />
    </div>
  );
};

export default CommentPanel;
