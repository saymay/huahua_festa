import React from "react";

import { IconButton, TextField } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import SendIcon from "@mui/icons-material/Send";

import { AuthState } from "@aws-amplify/ui-components";

import API from "@aws-amplify/api";
import { graphqlOperation } from "aws-amplify";
import { createCommentAndNotification } from "graphql/mutations";

import { Anchor } from "../Models";
import useGoAuth from "shared/functions/useGoAuth";

interface props {
  workId: string;
  toggleDrawer: (anchor: Anchor, open: boolean) => void;
  authState: AuthState;
  user: any;
}

const CreateComment = ({ workId, toggleDrawer, authState, user }: props) => {
  const goAuth = useGoAuth();

  // handle input text
  const [comment, setComment] = React.useState<string>("");
  const handleChange = (event: any) => {
    setComment(event.target.value);
  };

  // post comment
  const postComment = async () => {
    if (authState === AuthState.SignedIn) {
      const res = (await API.graphql(
        graphqlOperation(createCommentAndNotification, {
          workId: workId,
          username: user.username,
          content: comment,
          createdAt: Math.round(new Date().getTime() / 1000),
        })
      )) as any;
    } else {
      goAuth();
    }
  };

  // redirect to auth page if current user is not signedIn
  const checkAuth = () => {
    if (authState !== AuthState.SignedIn) {
      toggleDrawer("comment", false);
      goAuth();
    }
  };

  // styles
  const useStyles = makeStyles(() =>
    createStyles({
      createComment: {
        display: "flex",
      },
      textField: {
        flexGrow: 1,
      },
    })
  );
  const classes = useStyles();

  return (
    <div className={classes.createComment}>
      <TextField
        label="コメント"
        multiline
        value={comment}
        onChange={handleChange}
        className={classes.textField}
        onFocus={checkAuth}
      />
      <IconButton color="primary" onClick={postComment} size="large">
        <SendIcon />
      </IconButton>
    </div>
  );
};

export default CreateComment;
