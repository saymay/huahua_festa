import { useLocation, useParams } from "react-router";
import { Fab } from "@mui/material";
import { Work } from "API";
import { Anchor } from "../Models";
import CopyIcon from "./images/copy.png";
import TwitterIcon from "./images/twitter.png";
import FacebookIcon from "./images/facebook.png";
import LineIcon from "./images/line.png";

interface props {
  work: Work;
  toggleDrawer: (anchor: Anchor, open: boolean) => void;
  openSnackbar: (message: string) => void;
}

const ShareWork = ({ work, toggleDrawer, openSnackbar }: props) => {
  const location = useLocation();

  // get work id
  const { workId } = useParams<{ workId: string }>(); // URLパラメータからworkIdを取得

  // share params
  const url = `https://festa.huahua-online.com${location.pathname}`;
  const text = `${work.author}作 ${work.title}`;
  const hashtags = "huahuaArtFesta,supportYour44Artist";

  // share
  const copyURL = () => {
    navigator.clipboard.writeText(url);
    toggleDrawer("share", false);
    openSnackbar("クリップボードにコピーしました");
  };
  const shareTwitter = () => {
    const win = window.open(
      `https://twitter.com/intent/tweet?text=${text}&url=${url}&hashtags=${hashtags}`,
      "_blank"
    );
    win?.focus();
    toggleDrawer("share", false);
  };
  const shareFacebook = () => {
    const win = window.open(
      `https://www.facebook.com/sharer/sharer.php?u=${url}`,
      "_blank"
    );
    win?.focus();
    toggleDrawer("share", false);
  };
  const shareLine = () => {
    const win = window.open(
      `https://social-plugins.line.me/lineit/share?url=${url}`,
      "_blank"
    );
    win?.focus();
    toggleDrawer("share", false);
  };

  return (
    <div>
      <Fab sx={{ m: 1, backgroundColor: "#888888" }} onClick={copyURL}>
        <img src={CopyIcon} alt="copy" />
      </Fab>
      <Fab sx={{ m: 1, backgroundColor: "#1DA1F2" }} onClick={shareTwitter}>
        <img src={TwitterIcon} alt="twitter" />
      </Fab>
      <Fab sx={{ m: 1, backgroundColor: "#4267B2" }} onClick={shareFacebook}>
        <img src={FacebookIcon} alt="facebook" />
      </Fab>
      <Fab sx={{ m: 1, backgroundColor: "#00B900" }} onClick={shareLine}>
        <img src={LineIcon} alt="line" />
      </Fab>
    </div>
  );
};

export default ShareWork;
