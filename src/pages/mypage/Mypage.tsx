import { useEffect, useState } from "react";
import { IonContent, IonPage } from "@ionic/react";
import { Button, Container, Paper, Typography } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import { AmplifyS3Image } from "@aws-amplify/ui-react";
import { API, graphqlOperation } from "aws-amplify";
import { getWorkByAuthor } from "graphql/queries";
import { Work } from "API";
import Reactions from "shared/components/Reactions";
import Spacer from "shared/components/Spacer";
import useRouteTo from "shared/functions/useRouteTo";

interface props {
  user: any;
  t: (text: string) => string;
}

const useStyles = makeStyles(() =>
  createStyles({
    s3image: {
      "--width": "100%",
    },
  })
);

const Mypage = ({ user, t }: props) => {
  const classes = useStyles();
  const [work, setWork] = useState<Work>();
  const routeTo = useRouteTo();
  const [likeAmount, setLikeAmount] = useState<number>(0);
  const [commentAmount, setCommentAmount] = useState<number>(0);

  useEffect(() => {
    getWork();
  }, []);

  /**
   * @description 作品情報の取得
   *
   */
  const getWork = async () => {
    const res = (await API.graphql(
      graphqlOperation(getWorkByAuthor, {
        author: user.username,
        limit: 1,
      })
    )) as any;
    const work = res.data.getWorkByAuthor.items[0];
    setWork(work);
    if (work.likes && work.likes.items) {
      setLikeAmount(work.likes.items.length);
    }
    if (work.comments && work.comments.items) {
      setCommentAmount(work.comments.items.length);
    }
  };

  return (
    <IonPage>
      <IonContent fullscreen>
        <Spacer />
        <Container sx={{ height: "100%", textAlign: "center" }}>
          <Paper elevation={0} sx={{ p: 2 }}>
            <Typography>{user.username}</Typography>
          </Paper>
          <Paper elevation={0} sx={{ p: 1 }}>
            <div>
              {work && (
                <Button
                  variant="outlined"
                  size="large"
                  onClick={() => {
                    routeTo(`/work/${work.id}`);
                  }}
                  sx={{ mb: 2 }}
                >
                  {t("応募した作品を見る")}
                </Button>
              )}
            </div>
            <div>
              <Button
                variant="outlined"
                size="large"
                onClick={() => {
                  routeTo("/apply");
                }}
              >
                {t("応募内容を変更する")}
              </Button>
            </div>
          </Paper>
          <Paper elevation={0} sx={{ p: 5 }}>
            <Typography align="center" gutterBottom={true}>
              {work ? work.title : "作品が登録されていません"}
            </Typography>
            <Paper
              elevation={0}
              sx={{ width: "500px", maxWidth: "100%", margin: "auto" }}
            >
              {work && (
                <AmplifyS3Image
                  className={classes.s3image}
                  imgKey={work.imagePath}
                />
              )}
            </Paper>
            <Paper elevation={0} sx={{ p: 2 }}>
              <Reactions
                likes={likeAmount}
                comments={commentAmount}
                alignCenter={true}
              />
            </Paper>
          </Paper>
        </Container>
      </IonContent>
    </IonPage>
  );
};

export default Mypage;
