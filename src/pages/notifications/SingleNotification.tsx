import { Card, CardContent, Paper, Theme } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import timestampToDisplayTime from "shared/functions/timestampToDisplayTime";
import { Notification } from "API";
import useRouteTo from "shared/functions/useRouteTo";

interface props {
  nf: Notification;
}

const SingleNotification = ({ nf }: props) => {
  // styles
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      card: {
        margin: theme.spacing(1),
      },
      bold: {
        fontWeight: "bold",
      },
    })
  );
  const classes = useStyles();

  const routeTo = useRouteTo();
  const goToWork = () => {
    routeTo(`/work/${nf.workId}`);
  };

  return (
    <Card sx={{ m: 1 }} onClick={goToWork}>
      <CardContent>
        <Paper elevation={0} sx={{ fontSize: 13 }}>
          {timestampToDisplayTime(nf.createdAt)}
        </Paper>
        <div>
          <span className={classes.bold}>{nf.workTitle}</span>に
          {nf.type === "like" ? "いいね" : "コメント"}
          がつきました
        </div>
      </CardContent>
    </Card>
  );
};
export default SingleNotification;
