export interface Notification {
  id: string;
  name: string;
  workName: string;
  type: "like" | "comment";
  timestamp: number;
}
