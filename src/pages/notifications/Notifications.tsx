import { IonPage, IonContent } from "@ionic/react";
import { ListNotificationsByCreatedAtQuery, Notification } from "API";
import API, { graphqlOperation, GraphQLResult } from "@aws-amplify/api";
import { listNotificationsByCreatedAt } from "graphql/queries";
import { VFC, useState, useEffect } from "react";
import Spacer from "shared/components/Spacer";
import SingleNotification from "./SingleNotification";
import { Auth } from "aws-amplify";

interface props {
  user: any;
  setNfCount: (count: number) => any;
}
const Notifications: VFC<props> = ({ user, setNfCount }: props) => {
  const [notifications, setNotifications] = useState<Notification[]>([]);

  useEffect(() => {
    getNotifications();
    updateNotificationCount();
  }, [, user]);

  // 通知データの取得
  const getNotifications = async () => {
    if (!user) return;
    const res: GraphQLResult<ListNotificationsByCreatedAtQuery> =
      (await API.graphql(
        graphqlOperation(listNotificationsByCreatedAt, {
          username: user.username,
        })
      )) as GraphQLResult<ListNotificationsByCreatedAtQuery>;

    if (res.data) {
      const listQ: ListNotificationsByCreatedAtQuery = res.data;
      if (
        listQ.listNotificationsByCreatedAt &&
        listQ.listNotificationsByCreatedAt.items
      ) {
        const n = listQ.listNotificationsByCreatedAt.items.filter(
          (item) => item !== null
        ) as Notification[];
        setNotifications(n);
      }
    }
  };

  // 未読の通知数(notificationsAmount)をリセット
  const updateNotificationCount = async () => {
    if (user) {
      Auth.updateUserAttributes(user, {
        "custom:notificationsAmount": "0",
      });
      setNfCount(0);
    }
  };

  return (
    <IonPage>
      <IonContent fullscreen>
        <Spacer />
        {notifications.map((notification) => (
          <SingleNotification key={notification.id} nf={notification} />
        ))}
      </IonContent>
    </IonPage>
  );
};
export default Notifications;
