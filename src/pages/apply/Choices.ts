import { Choice } from "shared/Models";

export const frameChoices: Choice[] = [
  {
    label: "ナチュラル",
    value: "natural",
  },
  {
    label: "ダーク",
    value: "dark",
  },
  {
    label: "ブラック",
    value: "black",
  },
  {
    label: "ブルー",
    value: "blue",
  },
  {
    label: "ゴールド",
    value: "gold",
  },
  {
    label: "シルバー",
    value: "silver",
  },
];

export const bgChoices: Choice[] = [
  {
    label: "ブラウン",
    value: "#8b4513",
  },
  {
    label: "グレー",
    value: "#808080",
  },
  {
    label: "ホワイト",
    value: "#f8f8f8",
  },
  {
    label: "ブラック",
    value: "#000000",
  },
];

export const lightPositionChoices: Choice[] = [
  {
    label: "中央",
    value: "center",
  },
  {
    label: "上",
    value: "top",
  },
  {
    label: "下",
    value: "bottom",
  },
  {
    label: "右",
    value: "right",
  },
  {
    label: "左",
    value: "left",
  },
];
