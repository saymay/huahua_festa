import { ChangeEvent, useState } from "react";
import { useHistory } from "react-router";
import {
  Button,
  Drawer,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Slider,
  TextField,
  Typography,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import API from "@aws-amplify/api";
import { graphqlOperation } from "aws-amplify";
import { createWork, updateWork } from "graphql/mutations";
import SelectImage from "./SelectImage";
import { bgChoices, frameChoices, lightPositionChoices } from "./Choices";
import { ChoiceForm } from "./Models";
import useRouteTo from "shared/functions/useRouteTo";
import PreviewWork from "./PreviewWork";

interface props {
  user: any;
  workId: string;
  title: string;
  setTitle: (value: string) => void;
  imagePath: string;
  setImagePath: (value: string) => void;
  aspectRatio: number;
  setAspectRatio: (value: number) => void;
  frame: string;
  setFrame: (value: string) => void;
  background: string;
  setBackground: (value: string) => void;
  lightPosition: string;
  setLightPosition: (value: string) => void;
  lightIntensity: number;
  setLightIntensity: (value: number) => void;
  openSnackbar: (message: string) => void;
  t: (text: string) => string;
}

const EditWork = ({
  user,
  workId,
  title,
  setTitle,
  imagePath,
  setImagePath,
  aspectRatio,
  setAspectRatio,
  frame,
  setFrame,
  background,
  setBackground,
  lightPosition,
  setLightPosition,
  lightIntensity,
  setLightIntensity,
  openSnackbar,
  t,
}: props) => {
  // loading button
  const [isLoading, setIsLoading] = useState<boolean>(false);

  // handle forms
  const handleTitleChange = (event: ChangeEvent<{ value: unknown }>) => {
    setTitle(event.target.value as string);
  };
  const handleFrameChange = (event: SelectChangeEvent<{ value: unknown }>) => {
    setFrame(event.target.value as string);
  };
  const handleBgChange = (event: SelectChangeEvent<{ value: unknown }>) => {
    setBackground(event.target.value as string);
  };
  const handleLightPositionChange = (
    event: SelectChangeEvent<{ value: unknown }>
  ) => {
    setLightPosition(event.target.value as string);
  };
  const handleLightIntensityChange = (
    event: object,
    value: number | number[]
  ) => {
    if (!Array.isArray(value)) {
      setLightIntensity(value);
    }
  };

  // セレクトボックスの設定値
  const choiceForms: ChoiceForm[] = [
    {
      id: "frame",
      label: "フレーム",
      choices: frameChoices,
      variable: frame,
      handler: handleFrameChange,
    },
    {
      id: "background",
      label: "背景",
      choices: bgChoices,
      variable: background,
      handler: handleBgChange,
    },
    {
      id: "lightPosition",
      label: "ライト位置",
      choices: lightPositionChoices,
      variable: lightPosition,
      handler: handleLightPositionChange,
    },
  ];

  // routing
  const routeTo = useRouteTo();

  // send application
  const onPost = async () => {
    setIsLoading(true);
    if (workId === "") {
      await callCreateWork();
    } else {
      await callUpdateWork();
    }
    setIsLoading(false);
    routeTo("/");
  };

  // cancel
  const history = useHistory();
  const cancel = () => history.goBack();

  const callCreateWork = async () => {
    const res = (await API.graphql(
      graphqlOperation(createWork, {
        input: {
          type: "work",
          author: user.username,
          title: title,
          imagePath: imagePath,
          aspectRatio: aspectRatio,
          frame: frame,
          background: background,
          lightPosition: lightPosition,
          lightIntensity: lightIntensity,
        },
      })
    )) as any;
    if (res.data.createWork) {
      openSnackbar("応募しました");
    }
  };

  const callUpdateWork = async () => {
    const res = (await API.graphql(
      graphqlOperation(updateWork, {
        input: {
          id: workId,
          title: title,
          imagePath: imagePath,
          aspectRatio: aspectRatio,
          frame: frame,
          background: background,
          lightPosition: lightPosition,
          lightIntensity: lightIntensity,
        },
      })
    )) as any;
    if (res.data.updateWork) {
      openSnackbar("更新しました");
    }
  };

  // handle preview drawer
  const [drawersState, setDrawersState] = useState<boolean>(false);
  const toggleDrawer = (open: boolean) => {
    setDrawersState(open);
  };

  return (
    <div>
      {/* 作品名 */}
      <FormControl sx={{ width: "95%", m: 1 }}>
        <TextField
          label={t("作品名")}
          value={title}
          onChange={handleTitleChange}
        />
      </FormControl>
      {/* 画像選択 */}
      <FormControl sx={{ width: "95%", m: 1 }}>
        <SelectImage
          user={user}
          imagePath={imagePath}
          setImagePath={setImagePath}
          setAspectRatio={setAspectRatio}
          t={t}
        />
      </FormControl>
      {/* select boxes */}
      {choiceForms.map((choiceForm) => (
        <FormControl key={choiceForm.id} sx={{ width: "95%", m: 1 }}>
          <InputLabel id={choiceForm.id}>{t(choiceForm.label)}</InputLabel>
          <Select
            labelId={choiceForm.id}
            value={choiceForm.variable}
            onChange={choiceForm.handler}
          >
            {choiceForm.choices.map((choice) => (
              <MenuItem key={choice.label} value={choice.value}>
                {t(choice.label)}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      ))}
      {/* 光の強さ */}
      <FormControl sx={{ width: "95%", m: 1 }}>
        <Typography
          id="lightIntensity"
          sx={{ color: "rgba(0, 0, 0, 0.54)", fontSize: 13 }}
        >
          {t("光の強さ")}
        </Typography>
        <Slider
          value={lightIntensity}
          aria-labelledby="lightIntensity"
          valueLabelDisplay="auto"
          step={0.25}
          marks
          min={0.25}
          max={2.5}
          onChange={handleLightIntensityChange}
        />
      </FormControl>
      {/* buttons */}
      <Paper
        elevation={0}
        sx={{
          display: "flex",
          justifyContent: "space-between",
          mt: 3,
          mb: 2,
        }}
      >
        <Button variant="contained" color="error" onClick={cancel}>
          {t("キャンセル")}
        </Button>
        <Button
          variant="contained"
          color="secondary"
          disabled={!imagePath}
          onClick={() => toggleDrawer(true)}
        >
          {t("プレビュー")}
        </Button>
        <LoadingButton
          loading={isLoading}
          variant="contained"
          color="primary"
          disabled={
            !(
              title &&
              imagePath &&
              aspectRatio &&
              frame &&
              background &&
              lightPosition
            )
          }
          onClick={onPost}
        >
          {workId == "" ? t("応募する") : t("更新する")}
        </LoadingButton>
      </Paper>
      {/* プレビューの描画 */}
      <Drawer
        anchor="bottom"
        open={drawersState}
        onClose={() => {
          toggleDrawer(false);
        }}
      >
        <PreviewWork
          toggleDrawer={toggleDrawer}
          imagePath={imagePath}
          aspectRatio={aspectRatio}
          frame={frame}
          background={background}
          lightPosition={lightPosition}
          lightIntensity={lightIntensity}
        />
      </Drawer>
    </div>
  );
};
export default EditWork;
