import { Backdrop, Button, CircularProgress } from "@mui/material";
import { useState } from "react";
import WorkGraphic from "shared/components/work-graphic/WorkGraphic";

interface props {
  toggleDrawer: (value: boolean) => void;
  imagePath: string;
  aspectRatio: number;
  frame: string;
  background: string;
  lightPosition: string;
  lightIntensity: number;
}

const PreviewWork = ({
  toggleDrawer,
  imagePath,
  aspectRatio,
  frame,
  background,
  lightPosition,
  lightIntensity,
}: props) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  return (
    <div>
      {/* ローディング */}
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Button
        variant="outlined"
        onClick={() => {
          toggleDrawer(false);
        }}
        sx={{ m: 1 }}
      >
        プレビューを閉じる
      </Button>
      <WorkGraphic
        imagePath={imagePath}
        aspectRatio={aspectRatio}
        frame={frame}
        background={background}
        lightPosition={lightPosition}
        lightIntensity={lightIntensity}
        setIsLoading={setIsLoading}
      />
    </div>
  );
};
export default PreviewWork;
