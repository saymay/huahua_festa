import { Choice } from "shared/Models";

export interface ChoiceForm {
  id: string;
  label: string;
  choices: Choice[];
  variable: string;
  handler: (event: any) => void;
}
