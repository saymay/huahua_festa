import { useState } from "react";
import { Button } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import { Storage } from "aws-amplify";
import PreviewImage from "./PreviewImage";

interface props {
  user: any;
  imagePath: string;
  setImagePath: (path: string) => void;
  setAspectRatio: (value: number) => void;
  t: (text: string) => string;
}

const SelectImage = ({
  user,
  imagePath,
  setImagePath,
  setAspectRatio,
  t,
}: props) => {
  // handle click
  let inputElement: HTMLInputElement | null;
  const triggerClickInput = () => {
    if (inputElement) {
      inputElement.click();
    }
  };

  // 端末上での画像のパス
  const [localImagePath, setLocalImagePath] = useState<string>("");

  // preview image
  const handleImage = async (event: any) => {
    // --- remote image ---
    const file = event.target.files[0];
    // generate image file name
    const username = user.username;
    const extension = file.type.split("/")[1];
    const timestamp = Math.floor(Date.now() / 1000);
    const key = `${username}${timestamp}.${extension}`;
    // upload image file to s3
    const s3key = await uploadImage(key, file);
    setImagePath(s3key);
    // --- local image ---
    const path = URL.createObjectURL(file);
    setLocalImagePath(path);
    //
    // get image aspect ratio
    const image = new Image();
    image.onload = () => {
      const size = {
        width: image.naturalWidth,
        height: image.naturalHeight,
      };
      setAspectRatio(size.width / size.height);
      URL.revokeObjectURL(image.src);
    };
    image.src = URL.createObjectURL(file);
  };

  // upload image
  const uploadImage = async (key: string, file: File) => {
    await Storage.put(key, file, { level: "public" });
    return key;
  };

  // styles
  const useStyles = makeStyles(() =>
    createStyles({
      inputElement: {
        display: "none",
      },
    })
  );
  const classes = useStyles();

  return (
    <div>
      <Button variant="contained" onClick={triggerClickInput}>
        {t("作品画像を選択")}
      </Button>
      <input
        className={classes.inputElement}
        ref={(input) => (inputElement = input)}
        type="file"
        accept="image/*"
        onChange={handleImage}
      />
      {/* 画像が選択されたらローカルの画像を表示、そうでなければS3の画像を表示する */}
      {imagePath || localImagePath ? (
        <PreviewImage imagePath={imagePath} localImagePath={localImagePath} />
      ) : (
        "未選択"
      )}
    </div>
  );
};
export default SelectImage;
