import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import { AmplifyS3Image } from "@aws-amplify/ui-react";

interface props {
  imagePath: string;
  localImagePath: string;
}

const PreviewImage = ({ imagePath, localImagePath }: props) => {
  // styles
  const useStyles = makeStyles(() =>
    createStyles({
      s3image: {
        "--width": "100%",
      },
    })
  );
  const classes = useStyles();

  return (
    <div>
      {localImagePath ? (
        <img src={localImagePath} />
      ) : (
        <AmplifyS3Image className={classes.s3image} imgKey={imagePath} />
      )}
    </div>
  );
};

export default PreviewImage;
