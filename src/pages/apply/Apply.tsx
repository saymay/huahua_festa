import { useEffect, useState } from "react";
import { IonContent, IonPage } from "@ionic/react";
import { Container } from "@mui/material";
import { graphqlOperation } from "aws-amplify";
import API from "@aws-amplify/api";
import { getWorkByAuthor } from "graphql/queries";
import useRouteTo from "shared/functions/useRouteTo";
import Spacer from "shared/components/Spacer";
import EditWork from "./EditWork";

interface props {
  user: any;
  openSnackbar: (message: string) => void;
  t: (text: string) => string;
}

const Apply = ({ user, openSnackbar, t }: props) => {
  // edit work forms
  const [workId, setWorkId] = useState<string>("");
  const [title, setTitle] = useState<string>("");
  const [imagePath, setImagePath] = useState<string>("");
  const [aspectRatio, setAspectRatio] = useState<number>(1);
  const [frame, setFrame] = useState<string>("natural");
  const [background, setBackground] = useState<string>("");
  const [lightPosition, setLightPosition] = useState<string>("");
  const [lightIntensity, setLightIntensity] = useState<number>(1);

  const routeTo = useRouteTo();

  // get work data from backend
  useEffect(() => {
    const init = async () => {
      const work = await getWork();
      // 作品を応募済みであれば作品一覧にリダイレクト
      if (work) {
        openSnackbar("応募済みの作品は変更できません");
        routeTo("/works");
      }
    };
    init();
  }, []);

  // 応募済みのworkを取得
  const getWork = async () => {
    const author = user.username;
    const res = (await API.graphql(
      graphqlOperation(getWorkByAuthor, {
        author: author,
      })
    )) as any;
    const work = res.data.getWorkByAuthor.items[0];
    return work ? work : null;
  };

  return (
    <IonPage>
      <IonContent fullscreen>
        <Container>
          <Spacer />
          <EditWork
            user={user}
            workId={workId}
            title={title}
            setTitle={setTitle}
            imagePath={imagePath}
            setImagePath={setImagePath}
            aspectRatio={aspectRatio}
            setAspectRatio={setAspectRatio}
            frame={frame}
            setFrame={setFrame}
            background={background}
            setBackground={setBackground}
            lightPosition={lightPosition}
            setLightPosition={setLightPosition}
            lightIntensity={lightIntensity}
            setLightIntensity={setLightIntensity}
            openSnackbar={openSnackbar}
            t={t}
          />
        </Container>
      </IonContent>
    </IonPage>
  );
};
export default Apply;
