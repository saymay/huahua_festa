import { useEffect, useState, VFC } from "react";
import { IonContent, IonPage } from "@ionic/react";
import { Container, Grid, Paper } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import API, { graphqlOperation, GraphQLResult } from "@aws-amplify/api";
import { Work, ListWorksQuery } from "API";
import { listWorks } from "graphql/queries";
import useRouteTo from "shared/functions/useRouteTo";
import WorkThumbnail from "shared/components/WorkThumbnail";
import Spacer from "shared/components/Spacer";
import banner from "shared/assets/images/banner.jpg";
import huahuaLink from "shared/assets/images/huahua_link.jpg";
import { blue } from "@mui/material/colors";
import Award from "./Award";

interface props {
  t: (text: string) => string;
}

// 作品一覧の順序をシャッフル
const shuffle = ([...array]) => {
  for (let i = array.length - 1; i >= 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};

const Works = ({ t }: props) => {
  const [works, setWorks] = useState<Work[]>([]);

  useEffect(() => {
    getWorks();
  }, []);

  // 作品一覧をAPiから取得
  const getWorks = async () => {
    const res: GraphQLResult<ListWorksQuery> = (await API.graphql(
      graphqlOperation(listWorks)
    )) as GraphQLResult<ListWorksQuery>;
    if (res.data) {
      const listQ: ListWorksQuery = res.data;
      if (listQ.listWorks && listQ.listWorks.items) {
        const works = listQ.listWorks.items.filter(
          (item) => item !== null
        ) as Work[];
        setWorks(shuffle(works));
      }
    }
  };

  // go to outline page
  const routeTo = useRouteTo();
  const goToOutline = () => {
    routeTo("/outline");
  };

  // go to huahua regular exhibition
  const huahuaUrl = "https://www.huahua-online.com/";
  const goToHuahua = () => {
    window.open(huahuaUrl, "_blank");
  };

  // styles
  const useStyles = makeStyles(() =>
    createStyles({
      image: {
        width: "100%",
      },
    })
  );
  const classes = useStyles();

  return (
    <IonPage>
      <IonContent fullscreen>
        <Spacer />
        <Container>
          <Paper onClick={goToOutline} elevation={0}>
            <img
              src={banner}
              alt="HUAHUA Art Festival"
              className={classes.image}
            />
          </Paper>
          <Award t={t} />
          <Grid container spacing={2}>
            {works.map((work, index) => {
              return (
                <Grid key={index} item xs={6} md={3}>
                  <WorkThumbnail work={work} />
                </Grid>
              );
            })}
          </Grid>
          <Paper onClick={goToHuahua} elevation={0}>
            <img
              src={huahuaLink}
              alt="HUAHUA Art Festival"
              className={classes.image}
            />
          </Paper>
        </Container>
      </IonContent>
    </IonPage>
  );
};

export default Works;
