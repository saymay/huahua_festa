/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createLikeAndNotification = /* GraphQL */ `
  mutation CreateLikeAndNotification($workId: ID!, $username: String!) {
    createLikeAndNotification(workId: $workId, username: $username) {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;
export const createCommentAndNotification = /* GraphQL */ `
  mutation CreateCommentAndNotification(
    $workId: ID!
    $username: String!
    $content: String!
  ) {
    createCommentAndNotification(
      workId: $workId
      username: $username
      content: $content
    ) {
      id
      workId
      username
      content
      createdAt
      updatedAt
    }
  }
`;
export const createWork = /* GraphQL */ `
  mutation CreateWork(
    $input: CreateWorkInput!
    $condition: ModelWorkConditionInput
  ) {
    createWork(input: $input, condition: $condition) {
      type
      id
      author
      title
      imagePath
      aspectRatio
      frame
      background
      lightPosition
      lightIntensity
      createdAt
      updatedAt
      likes {
        items {
          workId
          username
          createdAt
          updatedAt
        }
        nextToken
      }
      comments {
        items {
          id
          workId
          username
          content
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const updateWork = /* GraphQL */ `
  mutation UpdateWork(
    $input: UpdateWorkInput!
    $condition: ModelWorkConditionInput
  ) {
    updateWork(input: $input, condition: $condition) {
      type
      id
      author
      title
      imagePath
      aspectRatio
      frame
      background
      lightPosition
      lightIntensity
      createdAt
      updatedAt
      likes {
        items {
          workId
          username
          createdAt
          updatedAt
        }
        nextToken
      }
      comments {
        items {
          id
          workId
          username
          content
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const deleteWork = /* GraphQL */ `
  mutation DeleteWork(
    $input: DeleteWorkInput!
    $condition: ModelWorkConditionInput
  ) {
    deleteWork(input: $input, condition: $condition) {
      type
      id
      author
      title
      imagePath
      aspectRatio
      frame
      background
      lightPosition
      lightIntensity
      createdAt
      updatedAt
      likes {
        items {
          workId
          username
          createdAt
          updatedAt
        }
        nextToken
      }
      comments {
        items {
          id
          workId
          username
          content
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const createLike = /* GraphQL */ `
  mutation CreateLike(
    $input: CreateLikeInput!
    $condition: ModelLikeConditionInput
  ) {
    createLike(input: $input, condition: $condition) {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;
export const updateLike = /* GraphQL */ `
  mutation UpdateLike(
    $input: UpdateLikeInput!
    $condition: ModelLikeConditionInput
  ) {
    updateLike(input: $input, condition: $condition) {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;
export const deleteLike = /* GraphQL */ `
  mutation DeleteLike(
    $input: DeleteLikeInput!
    $condition: ModelLikeConditionInput
  ) {
    deleteLike(input: $input, condition: $condition) {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;
export const createComment = /* GraphQL */ `
  mutation CreateComment(
    $input: CreateCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    createComment(input: $input, condition: $condition) {
      id
      workId
      username
      content
      createdAt
      updatedAt
    }
  }
`;
export const updateComment = /* GraphQL */ `
  mutation UpdateComment(
    $input: UpdateCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    updateComment(input: $input, condition: $condition) {
      id
      workId
      username
      content
      createdAt
      updatedAt
    }
  }
`;
export const deleteComment = /* GraphQL */ `
  mutation DeleteComment(
    $input: DeleteCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    deleteComment(input: $input, condition: $condition) {
      id
      workId
      username
      content
      createdAt
      updatedAt
    }
  }
`;
export const createNotification = /* GraphQL */ `
  mutation CreateNotification(
    $input: CreateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    createNotification(input: $input, condition: $condition) {
      id
      username
      createdAt
      workId
      workTitle
      from
      type
      updatedAt
    }
  }
`;
