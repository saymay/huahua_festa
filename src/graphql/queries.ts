/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getWork = /* GraphQL */ `
  query GetWork($id: ID!) {
    getWork(id: $id) {
      type
      id
      author
      title
      imagePath
      aspectRatio
      frame
      background
      lightPosition
      lightIntensity
      createdAt
      updatedAt
      likes {
        items {
          workId
          username
          createdAt
          updatedAt
        }
        nextToken
      }
      comments {
        items {
          id
          workId
          username
          content
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const listWorks = /* GraphQL */ `
  query ListWorks(
    $filter: ModelWorkFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listWorks(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        type
        id
        author
        title
        imagePath
        aspectRatio
        frame
        background
        lightPosition
        lightIntensity
        createdAt
        updatedAt
        likes {
          items {
            workId
            username
            createdAt
            updatedAt
          }
          nextToken
        }
        comments {
          items {
            id
            workId
            username
            content
            createdAt
            updatedAt
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;
export const listAllWorks = /* GraphQL */ `
  query ListAllWorks(
    $type: String
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelWorkFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listAllWorks(
      type: $type
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        type
        id
        author
        title
        imagePath
        aspectRatio
        frame
        background
        lightPosition
        lightIntensity
        createdAt
        updatedAt
        likes {
          items {
            workId
            username
            createdAt
            updatedAt
          }
          nextToken
        }
        comments {
          items {
            id
            workId
            username
            content
            createdAt
            updatedAt
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;
export const getWorkByAuthor = /* GraphQL */ `
  query GetWorkByAuthor(
    $author: String
    $sortDirection: ModelSortDirection
    $filter: ModelWorkFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getWorkByAuthor(
      author: $author
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        type
        id
        author
        title
        imagePath
        aspectRatio
        frame
        background
        lightPosition
        lightIntensity
        createdAt
        updatedAt
        likes {
          items {
            workId
            username
            createdAt
            updatedAt
          }
          nextToken
        }
        comments {
          items {
            id
            workId
            username
            content
            createdAt
            updatedAt
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;
export const getLike = /* GraphQL */ `
  query GetLike($workId: ID!, $username: String!) {
    getLike(workId: $workId, username: $username) {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;
export const listLikes = /* GraphQL */ `
  query ListLikes(
    $workId: ID
    $username: ModelStringKeyConditionInput
    $filter: ModelLikeFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listLikes(
      workId: $workId
      username: $username
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        workId
        username
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLikesByWorkId = /* GraphQL */ `
  query GetLikesByWorkId(
    $workId: ID
    $createdAt: ModelIntKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelLikeFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getLikesByWorkId(
      workId: $workId
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        workId
        username
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getComment = /* GraphQL */ `
  query GetComment($id: ID!) {
    getComment(id: $id) {
      id
      workId
      username
      content
      createdAt
      updatedAt
    }
  }
`;
export const listComments = /* GraphQL */ `
  query ListComments(
    $filter: ModelCommentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        workId
        username
        content
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const listCommentsByWorkId = /* GraphQL */ `
  query ListCommentsByWorkId(
    $workId: ID
    $createdAt: ModelIntKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCommentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCommentsByWorkId(
      workId: $workId
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        workId
        username
        content
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getNotification = /* GraphQL */ `
  query GetNotification($id: ID!) {
    getNotification(id: $id) {
      id
      username
      createdAt
      workId
      workTitle
      from
      type
      updatedAt
    }
  }
`;
export const listNotifications = /* GraphQL */ `
  query ListNotifications(
    $filter: ModelNotificationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listNotifications(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        username
        createdAt
        workId
        workTitle
        from
        type
        updatedAt
      }
      nextToken
    }
  }
`;
export const listNotificationsByCreatedAt = /* GraphQL */ `
  query ListNotificationsByCreatedAt(
    $username: String
    $createdAt: ModelIntKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelNotificationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listNotificationsByCreatedAt(
      username: $username
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        username
        createdAt
        workId
        workTitle
        from
        type
        updatedAt
      }
      nextToken
    }
  }
`;
