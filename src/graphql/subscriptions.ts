/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateWork = /* GraphQL */ `
  subscription OnCreateWork {
    onCreateWork {
      type
      id
      author
      title
      imagePath
      aspectRatio
      frame
      background
      lightPosition
      lightIntensity
      createdAt
      updatedAt
      likes {
        items {
          workId
          username
          createdAt
          updatedAt
        }
        nextToken
      }
      comments {
        items {
          id
          workId
          username
          content
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const onUpdateWork = /* GraphQL */ `
  subscription OnUpdateWork {
    onUpdateWork {
      type
      id
      author
      title
      imagePath
      aspectRatio
      frame
      background
      lightPosition
      lightIntensity
      createdAt
      updatedAt
      likes {
        items {
          workId
          username
          createdAt
          updatedAt
        }
        nextToken
      }
      comments {
        items {
          id
          workId
          username
          content
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const onDeleteWork = /* GraphQL */ `
  subscription OnDeleteWork {
    onDeleteWork {
      type
      id
      author
      title
      imagePath
      aspectRatio
      frame
      background
      lightPosition
      lightIntensity
      createdAt
      updatedAt
      likes {
        items {
          workId
          username
          createdAt
          updatedAt
        }
        nextToken
      }
      comments {
        items {
          id
          workId
          username
          content
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const onCreateLike = /* GraphQL */ `
  subscription OnCreateLike {
    onCreateLike {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateLike = /* GraphQL */ `
  subscription OnUpdateLike {
    onUpdateLike {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteLike = /* GraphQL */ `
  subscription OnDeleteLike {
    onDeleteLike {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;
export const onCreateComment = /* GraphQL */ `
  subscription OnCreateComment {
    onCreateComment {
      id
      workId
      username
      content
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateComment = /* GraphQL */ `
  subscription OnUpdateComment {
    onUpdateComment {
      id
      workId
      username
      content
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteComment = /* GraphQL */ `
  subscription OnDeleteComment {
    onDeleteComment {
      id
      workId
      username
      content
      createdAt
      updatedAt
    }
  }
`;
export const onCreateNotification = /* GraphQL */ `
  subscription OnCreateNotification {
    onCreateNotification {
      id
      username
      createdAt
      workId
      workTitle
      from
      type
      updatedAt
    }
  }
`;
