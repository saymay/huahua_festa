/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type Like = {
  __typename: "Like",
  workId: string,
  username: string,
  createdAt: number,
  updatedAt: string,
};

export type Comment = {
  __typename: "Comment",
  id: string,
  workId: string,
  username: string,
  content: string,
  createdAt: number,
  updatedAt: string,
};

export type CreateWorkInput = {
  type: string,
  id?: string | null,
  author: string,
  title: string,
  imagePath: string,
  aspectRatio: number,
  frame: string,
  background: string,
  lightPosition: string,
  lightIntensity?: number | null,
};

export type ModelWorkConditionInput = {
  type?: ModelStringInput | null,
  author?: ModelStringInput | null,
  title?: ModelStringInput | null,
  imagePath?: ModelStringInput | null,
  aspectRatio?: ModelFloatInput | null,
  frame?: ModelStringInput | null,
  background?: ModelStringInput | null,
  lightPosition?: ModelStringInput | null,
  lightIntensity?: ModelFloatInput | null,
  and?: Array< ModelWorkConditionInput | null > | null,
  or?: Array< ModelWorkConditionInput | null > | null,
  not?: ModelWorkConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelFloatInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type Work = {
  __typename: "Work",
  type: string,
  id?: string | null,
  author: string,
  title: string,
  imagePath: string,
  aspectRatio: number,
  frame: string,
  background: string,
  lightPosition: string,
  lightIntensity?: number | null,
  createdAt: string,
  updatedAt: string,
  likes?: ModelLikeConnection | null,
  comments?: ModelCommentConnection | null,
};

export type ModelLikeConnection = {
  __typename: "ModelLikeConnection",
  items?:  Array<Like | null > | null,
  nextToken?: string | null,
};

export type ModelCommentConnection = {
  __typename: "ModelCommentConnection",
  items?:  Array<Comment | null > | null,
  nextToken?: string | null,
};

export type UpdateWorkInput = {
  type?: string | null,
  id: string,
  author?: string | null,
  title?: string | null,
  imagePath?: string | null,
  aspectRatio?: number | null,
  frame?: string | null,
  background?: string | null,
  lightPosition?: string | null,
  lightIntensity?: number | null,
};

export type DeleteWorkInput = {
  id: string,
};

export type CreateLikeInput = {
  workId: string,
  username: string,
  createdAt: number,
};

export type ModelLikeConditionInput = {
  createdAt?: ModelIntInput | null,
  and?: Array< ModelLikeConditionInput | null > | null,
  or?: Array< ModelLikeConditionInput | null > | null,
  not?: ModelLikeConditionInput | null,
};

export type ModelIntInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type UpdateLikeInput = {
  workId: string,
  username: string,
  createdAt?: number | null,
};

export type DeleteLikeInput = {
  workId: string,
  username: string,
};

export type CreateCommentInput = {
  id?: string | null,
  workId: string,
  username: string,
  content: string,
  createdAt: number,
};

export type ModelCommentConditionInput = {
  workId?: ModelIDInput | null,
  username?: ModelStringInput | null,
  content?: ModelStringInput | null,
  createdAt?: ModelIntInput | null,
  and?: Array< ModelCommentConditionInput | null > | null,
  or?: Array< ModelCommentConditionInput | null > | null,
  not?: ModelCommentConditionInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type UpdateCommentInput = {
  workId?: string | null,
  username?: string | null,
  content?: string | null,
  createdAt?: number | null,
};

export type DeleteCommentInput = {
  id: string,
};

export type CreateNotificationInput = {
  id?: string | null,
  username: string,
  createdAt: number,
  workId: string,
  workTitle: string,
  from: string,
  type: string,
};

export type ModelNotificationConditionInput = {
  username?: ModelStringInput | null,
  createdAt?: ModelIntInput | null,
  workId?: ModelStringInput | null,
  workTitle?: ModelStringInput | null,
  from?: ModelStringInput | null,
  type?: ModelStringInput | null,
  and?: Array< ModelNotificationConditionInput | null > | null,
  or?: Array< ModelNotificationConditionInput | null > | null,
  not?: ModelNotificationConditionInput | null,
};

export type Notification = {
  __typename: "Notification",
  id?: string | null,
  username: string,
  createdAt: number,
  workId: string,
  workTitle: string,
  from: string,
  type: string,
  updatedAt: string,
};

export type ModelWorkFilterInput = {
  type?: ModelStringInput | null,
  id?: ModelIDInput | null,
  author?: ModelStringInput | null,
  title?: ModelStringInput | null,
  imagePath?: ModelStringInput | null,
  aspectRatio?: ModelFloatInput | null,
  frame?: ModelStringInput | null,
  background?: ModelStringInput | null,
  lightPosition?: ModelStringInput | null,
  lightIntensity?: ModelFloatInput | null,
  and?: Array< ModelWorkFilterInput | null > | null,
  or?: Array< ModelWorkFilterInput | null > | null,
  not?: ModelWorkFilterInput | null,
};

export type ModelWorkConnection = {
  __typename: "ModelWorkConnection",
  items?:  Array<Work | null > | null,
  nextToken?: string | null,
};

export type ModelIDKeyConditionInput = {
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}


export type ModelStringKeyConditionInput = {
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelLikeFilterInput = {
  workId?: ModelIDInput | null,
  username?: ModelStringInput | null,
  createdAt?: ModelIntInput | null,
  and?: Array< ModelLikeFilterInput | null > | null,
  or?: Array< ModelLikeFilterInput | null > | null,
  not?: ModelLikeFilterInput | null,
};

export type ModelIntKeyConditionInput = {
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelCommentFilterInput = {
  workId?: ModelIDInput | null,
  username?: ModelStringInput | null,
  content?: ModelStringInput | null,
  createdAt?: ModelIntInput | null,
  and?: Array< ModelCommentFilterInput | null > | null,
  or?: Array< ModelCommentFilterInput | null > | null,
  not?: ModelCommentFilterInput | null,
};

export type ModelNotificationFilterInput = {
  id?: ModelIDInput | null,
  username?: ModelStringInput | null,
  createdAt?: ModelIntInput | null,
  workId?: ModelStringInput | null,
  workTitle?: ModelStringInput | null,
  from?: ModelStringInput | null,
  type?: ModelStringInput | null,
  and?: Array< ModelNotificationFilterInput | null > | null,
  or?: Array< ModelNotificationFilterInput | null > | null,
  not?: ModelNotificationFilterInput | null,
};

export type ModelNotificationConnection = {
  __typename: "ModelNotificationConnection",
  items?:  Array<Notification | null > | null,
  nextToken?: string | null,
};

export type CreateLikeAndNotificationMutationVariables = {
  workId: string,
  username: string,
};

export type CreateLikeAndNotificationMutation = {
  createLikeAndNotification?:  {
    __typename: "Like",
    workId: string,
    username: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type CreateCommentAndNotificationMutationVariables = {
  workId: string,
  username: string,
  content: string,
};

export type CreateCommentAndNotificationMutation = {
  createCommentAndNotification?:  {
    __typename: "Comment",
    id: string,
    workId: string,
    username: string,
    content: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type CreateWorkMutationVariables = {
  input: CreateWorkInput,
  condition?: ModelWorkConditionInput | null,
};

export type CreateWorkMutation = {
  createWork?:  {
    __typename: "Work",
    type: string,
    id?: string | null,
    author: string,
    title: string,
    imagePath: string,
    aspectRatio: number,
    frame: string,
    background: string,
    lightPosition: string,
    lightIntensity?: number | null,
    createdAt: string,
    updatedAt: string,
    likes?:  {
      __typename: "ModelLikeConnection",
      items?:  Array< {
        __typename: "Like",
        workId: string,
        username: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
    comments?:  {
      __typename: "ModelCommentConnection",
      items?:  Array< {
        __typename: "Comment",
        id: string,
        workId: string,
        username: string,
        content: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
  } | null,
};

export type UpdateWorkMutationVariables = {
  input: UpdateWorkInput,
  condition?: ModelWorkConditionInput | null,
};

export type UpdateWorkMutation = {
  updateWork?:  {
    __typename: "Work",
    type: string,
    id?: string | null,
    author: string,
    title: string,
    imagePath: string,
    aspectRatio: number,
    frame: string,
    background: string,
    lightPosition: string,
    lightIntensity?: number | null,
    createdAt: string,
    updatedAt: string,
    likes?:  {
      __typename: "ModelLikeConnection",
      items?:  Array< {
        __typename: "Like",
        workId: string,
        username: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
    comments?:  {
      __typename: "ModelCommentConnection",
      items?:  Array< {
        __typename: "Comment",
        id: string,
        workId: string,
        username: string,
        content: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
  } | null,
};

export type DeleteWorkMutationVariables = {
  input: DeleteWorkInput,
  condition?: ModelWorkConditionInput | null,
};

export type DeleteWorkMutation = {
  deleteWork?:  {
    __typename: "Work",
    type: string,
    id?: string | null,
    author: string,
    title: string,
    imagePath: string,
    aspectRatio: number,
    frame: string,
    background: string,
    lightPosition: string,
    lightIntensity?: number | null,
    createdAt: string,
    updatedAt: string,
    likes?:  {
      __typename: "ModelLikeConnection",
      items?:  Array< {
        __typename: "Like",
        workId: string,
        username: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
    comments?:  {
      __typename: "ModelCommentConnection",
      items?:  Array< {
        __typename: "Comment",
        id: string,
        workId: string,
        username: string,
        content: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
  } | null,
};

export type CreateLikeMutationVariables = {
  input: CreateLikeInput,
  condition?: ModelLikeConditionInput | null,
};

export type CreateLikeMutation = {
  createLike?:  {
    __typename: "Like",
    workId: string,
    username: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type UpdateLikeMutationVariables = {
  input: UpdateLikeInput,
  condition?: ModelLikeConditionInput | null,
};

export type UpdateLikeMutation = {
  updateLike?:  {
    __typename: "Like",
    workId: string,
    username: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type DeleteLikeMutationVariables = {
  input: DeleteLikeInput,
  condition?: ModelLikeConditionInput | null,
};

export type DeleteLikeMutation = {
  deleteLike?:  {
    __typename: "Like",
    workId: string,
    username: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type CreateCommentMutationVariables = {
  input: CreateCommentInput,
  condition?: ModelCommentConditionInput | null,
};

export type CreateCommentMutation = {
  createComment?:  {
    __typename: "Comment",
    id: string,
    workId: string,
    username: string,
    content: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type UpdateCommentMutationVariables = {
  input: UpdateCommentInput,
  condition?: ModelCommentConditionInput | null,
};

export type UpdateCommentMutation = {
  updateComment?:  {
    __typename: "Comment",
    id: string,
    workId: string,
    username: string,
    content: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type DeleteCommentMutationVariables = {
  input: DeleteCommentInput,
  condition?: ModelCommentConditionInput | null,
};

export type DeleteCommentMutation = {
  deleteComment?:  {
    __typename: "Comment",
    id: string,
    workId: string,
    username: string,
    content: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type CreateNotificationMutationVariables = {
  input: CreateNotificationInput,
  condition?: ModelNotificationConditionInput | null,
};

export type CreateNotificationMutation = {
  createNotification?:  {
    __typename: "Notification",
    id?: string | null,
    username: string,
    createdAt: number,
    workId: string,
    workTitle: string,
    from: string,
    type: string,
    updatedAt: string,
  } | null,
};

export type GetWorkQueryVariables = {
  id: string,
};

export type GetWorkQuery = {
  getWork?:  {
    __typename: "Work",
    type: string,
    id?: string | null,
    author: string,
    title: string,
    imagePath: string,
    aspectRatio: number,
    frame: string,
    background: string,
    lightPosition: string,
    lightIntensity?: number | null,
    createdAt: string,
    updatedAt: string,
    likes?:  {
      __typename: "ModelLikeConnection",
      items?:  Array< {
        __typename: "Like",
        workId: string,
        username: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
    comments?:  {
      __typename: "ModelCommentConnection",
      items?:  Array< {
        __typename: "Comment",
        id: string,
        workId: string,
        username: string,
        content: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
  } | null,
};

export type ListWorksQueryVariables = {
  filter?: ModelWorkFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListWorksQuery = {
  listWorks?:  {
    __typename: "ModelWorkConnection",
    items?:  Array< {
      __typename: "Work",
      type: string,
      id?: string | null,
      author: string,
      title: string,
      imagePath: string,
      aspectRatio: number,
      frame: string,
      background: string,
      lightPosition: string,
      lightIntensity?: number | null,
      createdAt: string,
      updatedAt: string,
      likes?:  {
        __typename: "ModelLikeConnection",
        items?:  Array< {
          __typename: "Like",
          workId: string,
          username: string,
          createdAt: number,
          updatedAt: string,
        } | null > | null,
        nextToken?: string | null,
      } | null,
      comments?:  {
        __typename: "ModelCommentConnection",
        items?:  Array< {
          __typename: "Comment",
          id: string,
          workId: string,
          username: string,
          content: string,
          createdAt: number,
          updatedAt: string,
        } | null > | null,
        nextToken?: string | null,
      } | null,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type ListAllWorksQueryVariables = {
  type?: string | null,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelWorkFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListAllWorksQuery = {
  listAllWorks?:  {
    __typename: "ModelWorkConnection",
    items?:  Array< {
      __typename: "Work",
      type: string,
      id?: string | null,
      author: string,
      title: string,
      imagePath: string,
      aspectRatio: number,
      frame: string,
      background: string,
      lightPosition: string,
      lightIntensity?: number | null,
      createdAt: string,
      updatedAt: string,
      likes?:  {
        __typename: "ModelLikeConnection",
        items?:  Array< {
          __typename: "Like",
          workId: string,
          username: string,
          createdAt: number,
          updatedAt: string,
        } | null > | null,
        nextToken?: string | null,
      } | null,
      comments?:  {
        __typename: "ModelCommentConnection",
        items?:  Array< {
          __typename: "Comment",
          id: string,
          workId: string,
          username: string,
          content: string,
          createdAt: number,
          updatedAt: string,
        } | null > | null,
        nextToken?: string | null,
      } | null,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type GetWorkByAuthorQueryVariables = {
  author?: string | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelWorkFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type GetWorkByAuthorQuery = {
  getWorkByAuthor?:  {
    __typename: "ModelWorkConnection",
    items?:  Array< {
      __typename: "Work",
      type: string,
      id?: string | null,
      author: string,
      title: string,
      imagePath: string,
      aspectRatio: number,
      frame: string,
      background: string,
      lightPosition: string,
      lightIntensity?: number | null,
      createdAt: string,
      updatedAt: string,
      likes?:  {
        __typename: "ModelLikeConnection",
        items?:  Array< {
          __typename: "Like",
          workId: string,
          username: string,
          createdAt: number,
          updatedAt: string,
        } | null > | null,
        nextToken?: string | null,
      } | null,
      comments?:  {
        __typename: "ModelCommentConnection",
        items?:  Array< {
          __typename: "Comment",
          id: string,
          workId: string,
          username: string,
          content: string,
          createdAt: number,
          updatedAt: string,
        } | null > | null,
        nextToken?: string | null,
      } | null,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type GetLikeQueryVariables = {
  workId: string,
  username: string,
};

export type GetLikeQuery = {
  getLike?:  {
    __typename: "Like",
    workId: string,
    username: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type ListLikesQueryVariables = {
  workId?: string | null,
  username?: ModelStringKeyConditionInput | null,
  filter?: ModelLikeFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  sortDirection?: ModelSortDirection | null,
};

export type ListLikesQuery = {
  listLikes?:  {
    __typename: "ModelLikeConnection",
    items?:  Array< {
      __typename: "Like",
      workId: string,
      username: string,
      createdAt: number,
      updatedAt: string,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type GetLikesByWorkIdQueryVariables = {
  workId?: string | null,
  createdAt?: ModelIntKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelLikeFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type GetLikesByWorkIdQuery = {
  getLikesByWorkId?:  {
    __typename: "ModelLikeConnection",
    items?:  Array< {
      __typename: "Like",
      workId: string,
      username: string,
      createdAt: number,
      updatedAt: string,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type GetCommentQueryVariables = {
  id: string,
};

export type GetCommentQuery = {
  getComment?:  {
    __typename: "Comment",
    id: string,
    workId: string,
    username: string,
    content: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type ListCommentsQueryVariables = {
  filter?: ModelCommentFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCommentsQuery = {
  listComments?:  {
    __typename: "ModelCommentConnection",
    items?:  Array< {
      __typename: "Comment",
      id: string,
      workId: string,
      username: string,
      content: string,
      createdAt: number,
      updatedAt: string,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type ListCommentsByWorkIdQueryVariables = {
  workId?: string | null,
  createdAt?: ModelIntKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelCommentFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCommentsByWorkIdQuery = {
  listCommentsByWorkId?:  {
    __typename: "ModelCommentConnection",
    items?:  Array< {
      __typename: "Comment",
      id: string,
      workId: string,
      username: string,
      content: string,
      createdAt: number,
      updatedAt: string,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type GetNotificationQueryVariables = {
  id: string,
};

export type GetNotificationQuery = {
  getNotification?:  {
    __typename: "Notification",
    id?: string | null,
    username: string,
    createdAt: number,
    workId: string,
    workTitle: string,
    from: string,
    type: string,
    updatedAt: string,
  } | null,
};

export type ListNotificationsQueryVariables = {
  filter?: ModelNotificationFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListNotificationsQuery = {
  listNotifications?:  {
    __typename: "ModelNotificationConnection",
    items?:  Array< {
      __typename: "Notification",
      id?: string | null,
      username: string,
      createdAt: number,
      workId: string,
      workTitle: string,
      from: string,
      type: string,
      updatedAt: string,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type ListNotificationsByCreatedAtQueryVariables = {
  username?: string | null,
  createdAt?: ModelIntKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelNotificationFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListNotificationsByCreatedAtQuery = {
  listNotificationsByCreatedAt?:  {
    __typename: "ModelNotificationConnection",
    items?:  Array< {
      __typename: "Notification",
      id?: string | null,
      username: string,
      createdAt: number,
      workId: string,
      workTitle: string,
      from: string,
      type: string,
      updatedAt: string,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type OnCreateWorkSubscription = {
  onCreateWork?:  {
    __typename: "Work",
    type: string,
    id?: string | null,
    author: string,
    title: string,
    imagePath: string,
    aspectRatio: number,
    frame: string,
    background: string,
    lightPosition: string,
    lightIntensity?: number | null,
    createdAt: string,
    updatedAt: string,
    likes?:  {
      __typename: "ModelLikeConnection",
      items?:  Array< {
        __typename: "Like",
        workId: string,
        username: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
    comments?:  {
      __typename: "ModelCommentConnection",
      items?:  Array< {
        __typename: "Comment",
        id: string,
        workId: string,
        username: string,
        content: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
  } | null,
};

export type OnUpdateWorkSubscription = {
  onUpdateWork?:  {
    __typename: "Work",
    type: string,
    id?: string | null,
    author: string,
    title: string,
    imagePath: string,
    aspectRatio: number,
    frame: string,
    background: string,
    lightPosition: string,
    lightIntensity?: number | null,
    createdAt: string,
    updatedAt: string,
    likes?:  {
      __typename: "ModelLikeConnection",
      items?:  Array< {
        __typename: "Like",
        workId: string,
        username: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
    comments?:  {
      __typename: "ModelCommentConnection",
      items?:  Array< {
        __typename: "Comment",
        id: string,
        workId: string,
        username: string,
        content: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
  } | null,
};

export type OnDeleteWorkSubscription = {
  onDeleteWork?:  {
    __typename: "Work",
    type: string,
    id?: string | null,
    author: string,
    title: string,
    imagePath: string,
    aspectRatio: number,
    frame: string,
    background: string,
    lightPosition: string,
    lightIntensity?: number | null,
    createdAt: string,
    updatedAt: string,
    likes?:  {
      __typename: "ModelLikeConnection",
      items?:  Array< {
        __typename: "Like",
        workId: string,
        username: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
    comments?:  {
      __typename: "ModelCommentConnection",
      items?:  Array< {
        __typename: "Comment",
        id: string,
        workId: string,
        username: string,
        content: string,
        createdAt: number,
        updatedAt: string,
      } | null > | null,
      nextToken?: string | null,
    } | null,
  } | null,
};

export type OnCreateLikeSubscription = {
  onCreateLike?:  {
    __typename: "Like",
    workId: string,
    username: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type OnUpdateLikeSubscription = {
  onUpdateLike?:  {
    __typename: "Like",
    workId: string,
    username: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type OnDeleteLikeSubscription = {
  onDeleteLike?:  {
    __typename: "Like",
    workId: string,
    username: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type OnCreateCommentSubscription = {
  onCreateComment?:  {
    __typename: "Comment",
    id: string,
    workId: string,
    username: string,
    content: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type OnUpdateCommentSubscription = {
  onUpdateComment?:  {
    __typename: "Comment",
    id: string,
    workId: string,
    username: string,
    content: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type OnDeleteCommentSubscription = {
  onDeleteComment?:  {
    __typename: "Comment",
    id: string,
    workId: string,
    username: string,
    content: string,
    createdAt: number,
    updatedAt: string,
  } | null,
};

export type OnCreateNotificationSubscription = {
  onCreateNotification?:  {
    __typename: "Notification",
    id?: string | null,
    username: string,
    createdAt: number,
    workId: string,
    workTitle: string,
    from: string,
    type: string,
    updatedAt: string,
  } | null,
};
