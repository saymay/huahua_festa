import { useEffect } from "react";

import { IonContent, IonPage } from "@ionic/react";

import {
  AmplifyAuthContainer,
  AmplifyAuthenticator,
} from "@aws-amplify/ui-react";
import { AuthState, onAuthUIStateChange } from "@aws-amplify/ui-components";

import Spacer from "./Spacer";
import { useHistory, useLocation } from "react-router";
import SignUp from "./SignUp";

interface locationState {
  goBack: boolean;
  previousUrl: string;
}

const Authentication = () => {
  const history = useHistory();
  const location = useLocation();

  // watch auth state
  // redirect to previous page when auth state become "signedIn"
  useEffect(() => {
    return onAuthUIStateChange((nextAuthState) => {
      if (nextAuthState === AuthState.SignedIn) {
        const locationParam = location.state as locationState;
        if (locationParam.goBack) {
          history.push(locationParam.previousUrl);
        } else {
          history.push("/works");
        }
      }
    });
  }, []);

  return (
    <IonPage>
      <IonContent fullscreen>
        <Spacer />
        <AmplifyAuthContainer>
          <AmplifyAuthenticator>
            <SignUp />
          </AmplifyAuthenticator>
        </AmplifyAuthContainer>
      </IonContent>
    </IonPage>
  );
};

export default Authentication;
