import { createStyles, makeStyles } from "@mui/styles";

const Spacer = () => {
  // styles
  const useStyles = makeStyles(() =>
    createStyles({
      spacer: {
        paddingTop: 70,
      },
    })
  );
  const classes = useStyles();
  return <div className={classes.spacer}></div>;
};
export default Spacer;
