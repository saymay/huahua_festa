import * as THREE from "three";
import { useLoader } from "@react-three/fiber";
import { useEffect } from "react";

interface props {
  width: number;
  height: number;
  imagePath: any;
  framePath: any;
  setIsLoading: (isLoading: boolean) => void;
}

const Content = ({
  width,
  height,
  imagePath,
  framePath,
  setIsLoading,
}: props) => {
  // generate textures
  const image = useLoader(THREE.TextureLoader, imagePath) as THREE.Texture;
  const frame = useLoader(THREE.TextureLoader, framePath) as THREE.Texture;
  useEffect(() => {
    setIsLoading(false);
  }, []);

  return (
    <mesh>
      <boxGeometry args={[width, height, 0.1]} />
      <meshPhongMaterial attachArray="material" map={frame} />
      <meshPhongMaterial attachArray="material" map={frame} />
      <meshPhongMaterial attachArray="material" map={frame} />
      <meshPhongMaterial attachArray="material" map={frame} />
      <meshLambertMaterial attachArray="material" map={image} />
      <meshPhongMaterial attachArray="material" map={frame} />
    </mesh>
  );
};
export default Content;
