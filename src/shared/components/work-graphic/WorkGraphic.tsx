import { Suspense } from "react";

import { Vector3 } from "three";
import { Canvas } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";

import { makeStyles } from "@mui/styles";
import { Theme } from "@mui/material";

import createStyles from "@mui/styles/createStyles";

import Painting from "./Painting";

interface props {
  imagePath: string;
  aspectRatio: number; // 高さに対する横幅の比率
  frame: string;
  background: string;
  lightPosition: string;
  lightIntensity: number;
  setIsLoading: (isLoading: boolean) => void;
}

const lightPositions = [
  {
    name: "center",
    value: new Vector3(0, 0, 10),
  },
  {
    name: "top",
    value: new Vector3(0, 10, 10),
  },
  {
    name: "right",
    value: new Vector3(10, 0, 10),
  },
  {
    name: "bottom",
    value: new Vector3(0, -10, 10),
  },
  {
    name: "left",
    value: new Vector3(0, -10, 10),
  },
];

const WorkGraphic = ({
  imagePath,
  aspectRatio,
  frame,
  background,
  lightPosition,
  lightIntensity,
  setIsLoading,
}: props) => {
  // set light position
  const selectedLightPosition = lightPositions.find(
    (p) => p.name === lightPosition
  );
  const currentLightPosition = selectedLightPosition
    ? selectedLightPosition
    : lightPositions[0];

  // styles
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      canvas: {
        minHeight: 600,
      },
    })
  );
  const classes = useStyles();

  return (
    <Canvas
      camera={{ fov: 60, position: [0, 0, 10] }}
      className={classes.canvas}
    >
      {/* ライト */}
      <ambientLight />
      <pointLight
        intensity={lightIntensity}
        position={currentLightPosition.value}
      />
      {/* 作品 */}
      <Suspense fallback={null}>
        <Painting
          imagePath={imagePath}
          aspectRatio={aspectRatio}
          frame={frame}
          setIsLoading={setIsLoading}
        />
      </Suspense>
      {/* コントロール */}
      <OrbitControls />
      {/* 背景 */}
      <color attach="background" args={[background]} />
    </Canvas>
  );
};

export default WorkGraphic;
