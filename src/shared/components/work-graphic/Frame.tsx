import * as THREE from "three";
import { useLoader, Vector3 } from "@react-three/fiber";

interface props {
  param: {
    position: Vector3;
    width: number;
    height: number;
    depth: number;
  };
  framePath: string;
}

const Frame = ({ param, framePath }: props) => {
  const frame = useLoader(THREE.TextureLoader, framePath) as THREE.Texture;

  return (
    <mesh position={param.position}>
      <boxGeometry args={[param.width, param.height, param.depth]} />
      <meshPhongMaterial attach="material" map={frame} />
    </mesh>
  );
};
export default Frame;
