import { useEffect, useRef, useState } from "react";

import { Vector3 } from "three";

import Storage from "@aws-amplify/storage";

import Content from "./Content";
import Frame from "./Frame";
import natural from "./frames/natural.jpg";
import dark from "./frames/dark.jpg";
import black from "./frames/black.jpg";
import blue from "./frames/blue.jpg";
import gold from "./frames/gold.jpg";
import silver from "./frames/silver.jpg";

interface props {
  imagePath: string;
  aspectRatio: number;
  frame: string;
  setIsLoading: (isLoading: boolean) => void;
}

const maxLength = 5;
const frameThickness = 0.3;
const frames = [
  {
    name: "natural",
    path: natural,
  },
  {
    name: "dark",
    path: dark,
  },
  {
    name: "black",
    path: black,
  },
  {
    name: "blue",
    path: blue,
  },
  {
    name: "gold",
    path: gold,
  },
  {
    name: "silver",
    path: silver,
  },
];

const Painting = ({ imagePath, aspectRatio, frame, setIsLoading }: props) => {
  const group: any = useRef();

  // get image path
  const [s3ImagePath, sets3ImagePath] = useState<string>();
  useEffect(() => {
    const getImageTexture = async () => {
      const path = (await Storage.get(imagePath)) as string;
      sets3ImagePath(path);
    };
    getImageTexture();
  }, []);

  // set geometry size
  let contentWidth, contentHeight;
  if (aspectRatio > 1) {
    // 横幅の方が大きい場合
    contentWidth = maxLength;
    contentHeight = maxLength / aspectRatio;
  } else {
    contentHeight = maxLength;
    contentWidth = maxLength * aspectRatio;
  }

  // set frame params
  const frameParams = {
    top: {
      position: new Vector3(0, (contentHeight + frameThickness) / 2, 0),
      width: contentWidth + frameThickness * 2,
      height: frameThickness,
      depth: frameThickness,
    },
    right: {
      position: new Vector3((contentWidth + frameThickness) / 2, 0 / 2, 0),
      width: frameThickness,
      height: contentHeight + frameThickness * 2,
      depth: frameThickness,
    },
    bottom: {
      position: new Vector3(0, -(contentHeight + frameThickness) / 2, 0),
      width: contentWidth + frameThickness * 2,
      height: frameThickness,
      depth: frameThickness,
    },
    left: {
      position: new Vector3(-(contentWidth + frameThickness) / 2, 0 / 2, 0),
      width: frameThickness,
      height: contentHeight + frameThickness * 2,
      depth: frameThickness,
    },
  };

  // generate frame texture
  const selectedFramePath = frames.find((f) => f.name === frame)?.path;
  const framePath = selectedFramePath ? selectedFramePath : natural;
  return (
    <group position={[0, 0, 0]} ref={group}>
      {s3ImagePath && (
        <Content
          width={contentWidth}
          height={contentHeight}
          imagePath={s3ImagePath}
          framePath={framePath}
          setIsLoading={setIsLoading}
        />
      )}
      <Frame param={frameParams.top} framePath={framePath} />
      <Frame param={frameParams.right} framePath={framePath} />
      <Frame param={frameParams.bottom} framePath={framePath} />
      <Frame param={frameParams.left} framePath={framePath} />
    </group>
  );
};

export default Painting;
