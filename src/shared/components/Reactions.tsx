import { Theme } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import createStyles from "@mui/styles/createStyles";
import { red } from "@mui/material/colors";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";

interface ReactionsProps {
  likes: number;
  comments: number;
  alignCenter?: boolean;
}

// いいね数とコメント数の表示
const Reactions = ({
  likes,
  comments,
  alignCenter = false,
}: ReactionsProps) => {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      wrapper: {
        display: "flex",
        justifyContent: alignCenter ? "center" : "left",
      },
      reaction: {
        display: "flex",
        marginRight: theme.spacing(2),
      },
      icon: {
        marginRight: theme.spacing(0.5),
      },
      likeNumber: {
        color: red[500],
      },
    })
  );
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.reaction}>
        <FavoriteBorderIcon
          fontSize="small"
          sx={{ color: red[500] }}
          className={classes.icon}
        />
        <span className={classes.likeNumber}>{likes}</span>
      </div>
      <div className={classes.reaction}>
        <ChatBubbleOutlineIcon fontSize="small" className={classes.icon} />
        <span>{comments}</span>
      </div>
    </div>
  );
};
export default Reactions;
