import { Paper } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import { Theme } from "@mui/system";
import useRouteTo from "shared/functions/useRouteTo";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    text: {
      [theme.breakpoints.down("sm")]: {
        fontSize: "14px",
      },
    },
    link: {
      cursor: "pointer",
      textDecoration: "underline",
      display: "inline-block",
    },
  })
);

const AwardMessage = () => {
  const classes = useStyles();
  const routeTo = useRouteTo();

  return (
    <Paper elevation={0} className={classes.text}>
      <p>HUAHUA Art Festival 投票期間は終了いたしました.　</p>
      <p>
        <a
          className={classes.link}
          onClick={() => {
            routeTo("/award");
          }}
        >
          結果発表はこちら
        </a>
      </p>
    </Paper>
  );
};

export default AwardMessage;
