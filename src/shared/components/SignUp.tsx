import { AmplifySignUp } from "@aws-amplify/ui-react";
import { I18n } from "aws-amplify";
import { Translations } from "@aws-amplify/ui-components";

const SignUp = () => {
  return (
    <AmplifySignUp
      slot="sign-up"
      formFields={[
        {
          type: "username",
          label: I18n.get(Translations.USERNAME_LABEL),
          placeholder: "huahua",
          inputProps: { required: true, autocomplete: "username" },
        },
        {
          type: "email",
          label: I18n.get(Translations.EMAIL_LABEL),
          placeholder: "huahua@example.com",
          inputProps: { required: true, autocomplete: "email" },
        },
        {
          type: "password",
          label: I18n.get(Translations.PASSWORD_LABEL),
          placeholder: "",
          inputProps: {
            required: true,
            autocomplete: "current-password",
          },
        },
      ]}
    ></AmplifySignUp>
  );
};

export default SignUp;
