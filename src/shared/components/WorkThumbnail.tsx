import { useEffect, useState, VFC } from "react";
import { Theme, Card, LinearProgress } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import createStyles from "@mui/styles/createStyles";
import { AmplifyS3Image } from "@aws-amplify/ui-react";
import { Work } from "API";
import Reactions from "./Reactions";
import useRouteTo from "shared/functions/useRouteTo";

interface WorkThumbnailProps {
  work: Work;
}

// 作品一覧で表示される作品サムネイルカード
const WorkThumbnail: VFC<WorkThumbnailProps> = ({
  work,
}: WorkThumbnailProps) => {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      cardContainer: {
        margin: theme.spacing(0.5),
      },
      author: {
        fontSize: 14,
        color: "gray",
        marginTop: theme.spacing(0.3),
      },
    })
  );
  const classes = useStyles();
  const routeTo = useRouteTo();
  const [isLoading, setIsLoading] = useState(true);
  const [likeAmount, setLikeAmount] = useState<number>(0);
  const [commentAmount, setCommentAmount] = useState<number>(0);

  useEffect(() => {
    if (work.likes && work.likes.items) {
      setLikeAmount(work.likes.items.length);
    }
    if (work.comments && work.comments.items) {
      setCommentAmount(work.comments.items.length);
    }
  }, []);

  return (
    <div className={classes.cardContainer}>
      <Card
        sx={{
          height: {
            xs: "200px",
            md: "300px",
          },
        }}
        onClick={() => {
          routeTo(`/work/${work.id}`);
        }}
      >
        <AmplifyS3Image
          imgKey={work.imagePath}
          handleOnLoad={(e: any) => {
            // 画面サイズに合わせるために画像をリサイズ
            const imgElement = e.path[0];
            const ratio = imgElement.width / imgElement.height;
            if (ratio > 1) {
              // 横長の場合
              imgElement.style = `height: 300px; width: ${300 * ratio}px`;
            } else {
              // 縦長の場合
              imgElement.style = `height: auto; width: 100%`;
            }
            setIsLoading(false);
          }}
        />
      </Card>
      {isLoading && (
        <LinearProgress
          classes={{
            colorPrimary: "#ffffff",
            barColorPrimary: "#aaaaaa",
          }}
        />
      )}
      <div>
        <div className={classes.author}>{work.author}</div>
        <div>{work.title}</div>
        <div>
          <Reactions likes={likeAmount} comments={commentAmount} />
        </div>
      </div>
    </div>
  );
};

export default WorkThumbnail;
