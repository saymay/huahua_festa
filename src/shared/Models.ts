export interface Choice {
  label: string;
  value: string;
}
