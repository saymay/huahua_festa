import { useHistory, useLocation } from "react-router";

const useGoAuth = () => {
  const history = useHistory();
  const location = useLocation();
  const goAuth = () => {
    history.push("/auth", { goBack: true, previousUrl: location.pathname });
  };
  return goAuth;
};

export default useGoAuth;
