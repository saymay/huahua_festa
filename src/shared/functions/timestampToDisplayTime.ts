export default function timestampToDisplayTime(timestamp: number) {
  const dt = new Date(timestamp * 1000);
  return `${dt.getFullYear()}年${dt.getMonth() + 1}月${dt.getDate()}日 ${(
    "00" + dt.getHours()
  ).slice(-2)}:${("00" + dt.getMinutes()).slice(-2)}`;
}
