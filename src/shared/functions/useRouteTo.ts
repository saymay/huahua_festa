import { useHistory } from "react-router-dom";

// 関数routeToを返す
const useRouteTo = () => {
  const history = useHistory();
  // pathにroutingする
  const routeTo = (path: string) => {
    history.push(path);
  };
  return routeTo;
};

export default useRouteTo;
