/* Amplify Params - DO NOT EDIT
	API_HUAHUAFESTA_GRAPHQLAPIENDPOINTOUTPUT
	API_HUAHUAFESTA_GRAPHQLAPIIDOUTPUT
	API_HUAHUAFESTA_GRAPHQLAPIKEYOUTPUT
	AUTH_HUAHUAFESTA329181CD_USERPOOLID
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const AWSAppSyncClient = require("aws-appsync").default;
const gql = require("graphql-tag");
const AWS = require("aws-sdk");
global.fetch = require("node-fetch");
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

let graphqlClient;

exports.handler = async (event, context, callback) => {
  let env;
  let graphql_auth;

  // 入力値のバリデーション
  if (!event.arguments.workId) {
    callback("'workId' cannot be empty", null);
  }
  if (!event.arguments.username) {
    callback("'username' cannot be empty", null);
  }

  if (
    "AWS_EXECUTION_ENV" in process.env &&
    process.env.AWS_EXECUTION_ENV.indexOf("mock") === -1
  ) {
    //for cloud env
    env = process.env;
    graphql_auth = {
      type: "API_KEY",
      apiKey: env.API_HUAHUAFESTA_GRAPHQLAPIKEYOUTPUT,
    };
  } else {
    // for local mock
    env = {
      API_HUAHUAFESTA_GRAPHQLAPIENDPOINTOUTPUT:
        "http://localhost:20002/graphql",
      REGION: "us-east-1",
      AUTH_HUAHUAFESTA329181CD_USERPOOLID:
        process.env.AUTH_HUAHUAFESTA329181CD_USERPOOLID,
    };
    graphql_auth = {
      type: "API_KEY",
      apiKey: "da2-fakeApiId123456",
    };
  }

  if (!graphqlClient) {
    graphqlClient = new AWSAppSyncClient({
      url: env.API_HUAHUAFESTA_GRAPHQLAPIENDPOINTOUTPUT,
      region: env.REGION,
      auth: graphql_auth,
      disableOffline: true,
    });
  }

  // LikeされたWorkの作者usernameを取得する
  const workResult = await graphqlClient.query({
    query: gql(getWork),
    variables: {
      id: event.arguments.workId,
    },
  });
  const author = workResult.data.getWork.author;
  const workTitle = workResult.data.getWork.title;
  console.log("[work author]", author);

  // LikeのNotificationを取得
  const notificationResult = await graphqlClient.query({
    query: gql(listNotifications),
    variables: {
      type: "like",
      username: author,
      from: event.arguments.username,
    },
  });

  // Like作成
  const likeInput = {
    mutation: gql(createLike),
    variables: {
      input: {
        workId: event.arguments.workId,
        username: event.arguments.username,
        createdAt: Math.floor(Date.now() / 1000),
      },
    },
  };
  const res = await graphqlClient.mutate(likeInput);
  const like = res.data.createLike;
  console.log("[like created]", like);

  // すでに同じユーザからのLikeのNotificationが存在する場合は、新しいNotificationは作成しない
  // if (notificationResult.data.listNotificationsByCreatedAt?.items?.length > 0) {
  //   return like;
  // }

  // likeのNotification作成
  const notificationInput = {
    mutation: gql(createNotification),
    variables: {
      input: {
        username: author,
        createdAt: Math.floor(Date.now() / 1000),
        workId: event.arguments.workId,
        workTitle: workTitle,
        from: event.arguments.username,
        type: "like",
      },
    },
  };

  const notificationRes = await graphqlClient.mutate(notificationInput);
  const notification = notificationRes.data.createNotification;
  await updateCognitoUserNotificationCount(author, env);

  console.log("[notification created]", notification);

  return like;
};

const listNotifications = /* GraphQL */ `
  query listNotifications($username: String!, $from: String!, $type: String) {
    listNotificationsByCreatedAt(
      username: $username
      filter: { from: { eq: $from }, type: { eq: $type } }
    ) {
      nextToken
      items {
        from
        username
        type
      }
    }
  }
`;

const getWork = /* GraphQL */ `
  query GetWork($id: ID!) {
    getWork(id: $id) {
      id
      author
      title
    }
  }
`;

const createLike = /* GraphQL */ `
  mutation CreateLike(
    $input: CreateLikeInput!
    $condition: ModelLikeConditionInput
  ) {
    createLike(input: $input, condition: $condition) {
      workId
      username
      createdAt
      updatedAt
    }
  }
`;

const createNotification = /* GraphQL */ `
  mutation CreateNotification(
    $input: CreateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    createNotification(input: $input, condition: $condition) {
      id
      username
      createdAt
      workId
      workTitle
      from
      type
    }
  }
`;

const updateCognitoUserNotificationCount = async (username, env) => {
  const user = await cognitoidentityserviceprovider
    .adminGetUser(
      {
        UserPoolId: env.AUTH_HUAHUAFESTA329181CD_USERPOOLID,
        Username: username,
      },
      (err, data) => {
        if (err) {
          console.error(err);
        } else {
          console.log(data);
        }
      }
    )
    .promise();

  const currentObject = user.UserAttributes.find(
    (ojb) => ojb.Name === "custom:notificationsAmount"
  );
  const nfCounts =
    currentObject !== undefined ? Number(currentObject.Value) + 1 : 1;

  await cognitoidentityserviceprovider
    .adminUpdateUserAttributes(
      {
        UserAttributes: [
          {
            Name: "custom:notificationsAmount",
            Value: nfCounts.toString(),
          },
        ],
        UserPoolId: env.AUTH_HUAHUAFESTA329181CD_USERPOOLID,
        Username: username,
      },
      (err, data) => {
        if (err) console.log(err, err.stack);
        else console.log(data);
      }
    )
    .promise();
};
